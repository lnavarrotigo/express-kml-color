var fs = require('fs');



module.exports = function (app) {

  var Reporte = require("./models").reporteModel;

  // Esta funcion clona un objeto con sintaxis JSON
  function CLONE (obj) {
    return JSON.parse(JSON.stringify(obj));
  }

  // objetos
  var kmIni = '<?xml version="1.0" encoding="UTF-8"?>'
  +'<kml xmlns="http://www.opengis.net/kml/2.2" '
  +'xmlns:gx="http://www.google.com/kml/ext/2.2" '
  +'xmlns:kml="http://www.opengis.net/kml/2.2" '
  +'xmlns:atom="http://www.w3.org/2005/Atom"><Document>';

  var kmName = '';

  /*
  ** esta funcion debe devolver un conjunto de lineas xml
  ** necesarias para el doc.kml.
  ** devuelve texto en string.
  */
  function kmOpenStyleStr () {
    var prom = new Promise( function (resolve, reject) {
      fs.readFile('./img4kmz/objects/kmOpenStyle.xml',
      'utf8', function(err, data) {
        if (err) reject(err);
        //console.log(data);
        resolve(data);
      });
    });
    return prom;
  }

  /*
  * Route:: /kmGenerateDoc/:idreporte/:name
  * Actualiza los campos :folder y :kml de un documento "Reporte",
  * Sirve para que cada reporte, ya sea desde el paso 1 o el paso 4
  * se termine de completar dichos campos, para que pueda ser utilizado
  * el otro endpoint dedicado a generar el doc.kml completo a partir de toda
  * la data necesaria almacenada en un documento Reporte.
  * return: {success:true, src:'path/doc.kml', folder:'path/folder-random'}
  */
  app.get("/kmGenerateDoc/:idreporte/:name", function (req, res, next) {
    var name = req.params.name,
    idreporte= req.params.idreporte;
    if (name && idreporte) {
      fs.mkdtemp('img4kmz/'+idreporte+'-', function (err, folder) {
        if (err) { res.json({success:false}); }
        else {
          // folder Prints: /tmp/foo-itXde2
          console.log(folder);
          var archivo = fs.createWriteStream(folder+'/doc.kml', {'flags':'w'});
          archivo.write(kmIni);
          archivo.write('<name>'+name+'</name>');
          //archivo.write(kmOpenStyle());
          fs.readFile('./img4kmz/objects/kmOpenStyle.xml',
          'utf8', function(err, data) {
            if (err) {
              res.json({success:false});
            } else {
              archivo.write(data);
              archivo.write('<Folder>');
              archivo.write('<name>'+name+'</name>');
              archivo.write('<open>1</open>');
              //contenido
              archivo.end('</Folder></Folder></Document></kml>');

              Reporte.findOneAndUpdate({_id:idreporte},
              {$set: { folder:folder, kml:folder+'/doc.kml' }}, function (err, doc) {
                if (err) {
                  res.json({success:false});
                } else {
                  res.json({success:true, src:folder+'/doc.kml', folder:folder});
                }
              });

            };

          });


        }

      });
    } else {
      res.json({success:false}); // no vino name o idreporte
    }
  }); // route:: /kmGenerateDoc/:idreporte/:name



}; // fin module.exports
