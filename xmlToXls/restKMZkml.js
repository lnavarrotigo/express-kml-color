/*
* Este archivo provee un conjunto de endpoints y funciones que implementan por medio de
* promises, las funciones para leer archivos y generar el contenido de un documento kml
* para el reporte KMZ a devolver al cliente.
* Las funciones que implementan promises, estaran anidadas para manejar la asincronia
* de javascript.
*/

var fs = require('fs');
var js2xmlparser = require("js2xmlparser");
var archiver = require('archiver');
var path = require("path");

module.exports = function (app) {

  // Esta funcion clona un objeto con sintaxis JSON
  function CLONE (obj) {
    return JSON.parse(JSON.stringify(obj));
  }
  function getXmlStr (obj) {
    var myobj = js2xmlparser.parse("Placemark", obj);
    myobj = myobj.replace("<?xml version='1.0'?>","");
    return myobj;
  }

  var Reporte = require("./models").reporteModel;

  // objetos
  var kmlIni = '<?xml version="1.0" encoding="UTF-8"?>'
  +'<kml xmlns="http://www.opengis.net/kml/2.2" '
  +'xmlns:gx="http://www.google.com/kml/ext/2.2" '
  +'xmlns:kml="http://www.opengis.net/kml/2.2" '
  +'xmlns:atom="http://www.w3.org/2005/Atom"><Document>';

  var kmlFin = "</Folder></Document></kml>";

  var placemark = {
    "name" : "",
    "description" : "",
    "LookAt" : {
      "longitude" : "",
      "latitude" : "",
      "altitude" : "0",
      "heading" : "",
      "tilt" : "0",
      "range" : "",
      "gx:altitudeMode" : "relativeToSeaFloor",
    },
    "styleUrl" : "",
    "Point" : {
      "gx:drawOrder" : "1",
      "coordinates" : ""
    }
  };

  var cdataTag = '';

  var imgTag = '<img alt="" src="sustituir" height="300" width="400">';

  var placemark_hilos = {
    "name" : "",
    "description" : "",
    "styleUrl" : "",
    "LineString" : {
      "tessellate" : "1",
      "coordinates" : ""
    }
  };

  var folderhilos = {
    "name" : "",
    "open" : "1",

  };

  function traerReporte(idreporte) {
    var prom = new Promise( function (resolve, reject) {
      Reporte.findById(idreporte, function (err, reporte) {
        if (err) {
          reject(err);
        } else {
          resolve(reporte);
        }
      })
    });
    return prom;
  }
  function traerTextoInicioKml () {
    var prom = new Promise( function (resolve, reject) {
      fs.readFile('./img4kmz/objects/kmOpenStyle.xml',
      'utf8', function(err, data) {
        if (err) reject(err);
        //console.log(data);
        resolve(data);
      });
    });
    return prom;
  }
  function guardarKml(reporte_, textoInicioKml, kmlName) {
    var prom = new Promise ( function (resolve, reject) {
      var kfile = fs.createWriteStream(reporte_.kml, {'flags':'w'});
      kfile.write(kmlIni);
      kfile.write(kmlName);
      kfile.write(textoInicioKml);
      var cablePoints = {
        home:"",calle:"",postes:[], mufa:"", homeID :"", mufaID:""
      };

      var tbl = "";
      reporte_.listaPostes.forEach( function (item) {

          var placeitem = CLONE (placemark);
          placeitem["name"] = item.id;
          placeitem["LookAt"].longitude = item.longitude;
          placeitem["LookAt"].latitude = item.latitude;
          placeitem["Point"].coordinates=item.longitude+","+item.latitude+",0";

        tbl = "<table border='1' padding='0'>";

        switch (item.tipo1) {
          case 'Mufa':
            placeitem["styleUrl"] = "#msn_square";
            cablePoints.mufa = item.longitude+","+item.latitude+",0 ";
            cablePoints.mufaID = item.id;
            // preparo la tablita para la info de la mufa
            tbl += "<tr><td>ID ORIGEN</td><td>ID DESTINO</td><td>PUERTO</td><td>ID LARGO</td></tr>";
            tbl += "<tr>";
            tbl += "<td>"+reporte_.idmufa+"</td>";
            tbl += "<td>"+reporte_.instalacion+" "+reporte_.codigoPuertoSplitter+"</td>";
            tbl += "<td>"+reporte_.codigoPuertoSplitter.toUpperCase().replace("W","")+"</td>";
            tbl += "<td>"+reporte_.codigoLargoMufaNodo+"</td>";
            tbl += "</tr>";
            break;
          case 'Cliente':
            placeitem["styleUrl"] = "#msn_ranger_station";
            cablePoints.home = item.longitude+","+item.latitude+",0 ";
            cablePoints.homeID = item.id;
            //if (cablePoints.home)
            // preparo la tablita para la info del cliente
            tbl += "<tr><td>No INSTALACION/ID</td><td>FECHA INSTALACION </td><td>DIRECCION</td></tr>";
            tbl += "<tr>";
            tbl += "<td>"+reporte_.instalacion+"</td>";
            var finst = new Date(reporte_.fechaInstalacion);
            finst = ""+finst.getDate()+"-"+(finst.getMonth()+1)+"-"+finst.getFullYear();
            tbl += "<td>"+finst+"</td>";
            //tbl += "<td>"+reporte_.fechaInstalacion+"</td>";
            tbl += "<td>"+reporte_.direccion+"</td>";
            tbl += "</tr>";
            break;
          case 'Cable':
            placeitem = CLONE (placemark_hilos);
            console.log('vino cable');
            placeitem["styleUrl"] = "#m_ylw-pushpin";
            //cablePoints.calle = item.longitude+","+item.latitude+",0";
            placeitem["name"] = "("+cablePoints.mufaID+") - ("+ cablePoints.homeID+")";
            //placeitem["LineString"].coordinates = cablePoints.home +item.longitude+","+item.latitude+",0 ";
            var coord_ = "";
            cablePoints.postes.reverse();
            cablePoints.postes.forEach( function (posteitem) {
              coord_ = coord_+posteitem.poste;
            });
            placeitem["LineString"].coordinates = cablePoints.home+coord_+cablePoints.mufa;

            // preparo la tablita para la info del cable
            tbl += "<tr><td>PROCEDENCIA</td><td>DESTINO</td><td>HILOS PROCEDENCIA</td><td>HILOS DESTINO</td><td>SECUENCIA INICIAL</td><td>SECUENCIA FINAL</td></tr>";
            tbl += "<tr>";
            tbl += "<td>"+reporte_.idmufa+"</td>";
            tbl += "<td>"+reporte_.instalacion+" "+reporte_.nombreCliente+"</td>";
            // esta linea cambio abajo
            tbl += "<td>"+reporte_.hiloProcedencia+"</td><td>"+reporte_.hiloDestino+"</td>";
            tbl += "<td>"+reporte_.secuenciaInicial+"</td>";
            tbl += "<td>"+reporte_.secuenciaFinal+"</td>";
            tbl += "</tr>";
            break;
          case 'Poste':
            placeitem["styleUrl"] = "#msn_placemark_circle";
            cablePoints.postes.push({
              posteID : item.id,
              poste: item.longitude+","+item.latitude+",0 "
            });
            break;
          default:
        }

        var cd_ = '<![CDATA[<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"><html><head><meta content="text/html; charset=ISO-8859-1" http-equiv="content-type"><title>';
        cd_ += item.id+'</title></head><body>';
        if (item.images) {
          if (item.images.img1 && item.images.img1.length>0) {
            cd_ += '<img alt="" src="'+item.images.img1+'" height="300" width="400">';
          }
          if (item.images.img2 && item.images.img2.length>0) {
            cd_ += '<img alt="" src="'+item.images.img2+'" height="300" width="400">';
          }
          if (item.images.img3 && item.images.img3.length>0) {
            cd_ += '<img alt="" src="'+item.images.img3+'" height="300" width="400">';
          }
          if (item.images.img4 && item.images.img4.length>0) {
            cd_ += '<img alt="" src="'+item.images.img4+'" height="300" width="400">';
          }
          if (item.images.img5 && item.images.img5.length>0) {
            cd_ += '<img alt="" src="'+item.images.img5+'" height="300" width="400">';
          }
          if (item.images.img6 && item.images.img6.length>0) {
            cd_ += '<img alt="" src="'+item.images.img6+'" height="300" width="400">';
          }
          if (item.images.img7 && item.images.img7.length>0) {
            cd_ += '<img alt="" src="'+item.images.img7+'" height="300" width="400">';
          }
        }
        // tbl trae la sintaxis de <table>...
        tbl += "</table>";
        if (item.notas) {
          cd_ += tbl +'</body><br>Nota:'+item.notas+'<br></html>]]>';
        } else {
          cd_ += tbl +'</body></html>]]>';
        }
        placeitem["description"] =cd_;
        kfile.write(getXmlStr(placeitem));

      }); // fin forEach( function (item) {

      if (cablePoints.postes && cablePoints.postes.length>0) {
        cablePoints.postes.forEach( function (poste) {

        });
      }

      kfile.end(kmlFin);
      kfile.on('finish', function () {
        console.log('previo a dar resolve a guardarKml');
        var dat = {id:reporte_._id, fold:reporte_.folder};
        console.log(dat);
        resolve(dat);
      });

    }); // fin new Promise ( function (resolve, reject) {
    return prom;
  }
  function folderToZip(idreporte, folder) {
    var prom = new Promise( function (resolve, reject) {
      var fileName =   './img4kmz/downloads/'+idreporte+'.kmz';
      console.log('kmz ::', fileName);
      console.log('folder ::', folder);
      var fileOutput = fs.createWriteStream(fileName);
      var archive = archiver('zip');
      fileOutput.on('close', function () {
          console.log(archive.pointer() + ' total bytes');
          console.log('archiver has been finalized and the output file descriptor has closed.');
          //res.json({success:true});
          resolve(''+idreporte+'.kmz');
      });

      archive.pipe(fileOutput);
      archive.glob(""+folder+"/**/*"); //some glob pattern here
      //archive.glob("../dist/.htaccess"); //another glob pattern
      // add as many as you like
      archive.on('error', function(err){
          reject(err);
      });
      archive.finalize();
    });
    return prom;
  }

  /*
  * Route:: /apikmz/download/:idreporte
  * Genera todos los componentes necesarios para
  * mandar a comprimir una carpeta con fotos, kml
  * produciento el archivo .kmz que se devolvera al usuario.
  */
  app.get("/apikmz/download/:idreporte", function (req, res, next) {
    var idreporte = req.params.idreporte;
    var reporte_ = null,
        textoInicioKml = "",
        kmlName = "";

    console.log('--/traerReporte');
    traerReporte(idreporte)
      .then( function (reporte) {
        reporte_ = reporte;
        kmlName = "<name>"+reporte_.instalacion+"_"+reporte_.nombreCliente+".kml</name>";
        console.log('--/traerTextoInicioKml');
        return traerTextoInicioKml();
      })
      .then( function (texto) {
        textoInicioKml = texto;
        console.log('--/guardarKml con reporte:', reporte_._id);
        console.log(' reporte_.kml :: ', reporte_.kml);
        return guardarKml(reporte_, textoInicioKml, kmlName);
        /*var kfile = fs.createWriteStream(reporte_.kml, {'flags':'w'});
        kfile.write(kmlIni);
        kfile.write(kmlName);
        kfile.write(textoInicioKml);
        reporte_.listaPostes.forEach( function (item) {
          var placeitem = CLONE(placemark);
          placeitem["name"] = item.id;
          placeitem["LookAt"].longitude = item.longitude;
          placeitem["LookAt"].latitude = item.latitude;
          placeitem["styleUrl"] = "#msn_square";
          placeitem["Point"].coordinates=item.longitude+","+item.latitude+",0";
          kfile.write(getXmlStr(placeitem));
        });
        kfile.end(kmlFin);
        res.json({success:true, pathKmz: reporte_.kml});*/
        //console.log('--/folderToZip');
        //return folderToZip(reporte_);
      })
      .then( function (obj) {
        console.log(' a generar zip ', obj.id, ' fold: ', obj.fold);
        return folderToZip(obj.id, obj.fold);
      })
      .then( function (pathKmz) {
        res.json({success:true, pathKmz: pathKmz});
      })
      .catch( function (error) {
        res.json({mensaje:error.message});
      });
  });

  function borrarArchivo(filename) {
    var prom = new Promise( function (resolve, reject) {
      fs.unlink ( filename, function (err) {
         if (err) {
             reject(err);
         }
         resolve({success:true});
      });
    });

    return prom;
  }

  /*
  * Route:: /apikmz/unlinkFile
  * Recibe por body el nombre del archivo
  * Devuelve json con true/false
  */
  app.post("/apikmz/unlinkFile", function (req, res, next) {
    var filename = req.body.filename;
    console.log("archivo antes de borrar:",filename);
    borrarArchivo(filename)
    .then( function (response_data) {
      res.json(response_data);
    })
    .catch(function (err) {
      res.json({success:false});
    });
  }); // fin Route:: /apikmz/unlinkFile

// promise para leer todos los archivos de un directorio
function listadoArchivosDirectorio (ruta) {
    var prom = new Promise( function (resolve, reject) {
      //ruta es el path completo para un cierto directorio
      var filelist = [];asdf
      fs.readdir(p, function (err, files) {
          if (err) {
              throw err;
          }

          files.map(function (file) {
              return path.join(p, file);
          }).filter(function (file) {
              return fs.statSync(file).isFile();
          }).forEach(function (file) {
              filelist.push({ archivo:""+path.basename(file), ruta:file });
              console.log("%s (%s)", file, path.basename(file));

          });
          res.json({fileList:filelist});
      });
    });
};

}; // fin module.exports = function (app)
