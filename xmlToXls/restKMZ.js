var path = require('path');
var formidable = require('formidable');
var fs = require('fs');
var archiver = require('archiver');

module.exports = function (app) {

  // Esta funcion clona un objeto con sintaxis JSON
  function CLONE (obj) {
    return JSON.parse(JSON.stringify(obj));
  }

  var Reporte = require("./models").reporteModel;

  app.get("/apikmz/reporte", function (req, res, next) {
    try {
      Reporte.find({}, function (err, reportes) {
        if (err) {
          res.json({success:false});
        } else {
          res.json({success:true, reportes:reportes});
        }
      });
    } catch (e) {
      res.json({success:false});
    }
  });

  app.post("/apikmz/reporte", function (req, res, next) {

      var nuevoReporte = new Reporte(req.body);
      var fecha = req.body.fechaInstalacion.split("/");
      fecha =  new Date(fecha[2], fecha[1] -1, fecha[0]);
      nuevoReporte.fechaInstalacion = fecha;
      console.log(nuevoReporte);
      nuevoReporte.save( function (err) {
        if (err) {
          res.json({success:false});
        } else {
          res.json({success:true, reporte: nuevoReporte});
        }

      });
  // -- fin de POST "/apikmz/reporte"
  });

  app.get("/apikmz/reporte/:id", function (req, res, next) {
    try {
      var id = req.params.id;
      Reporte.findById(id, function (err, reporte) {
        if (err) {
          res.json({success:false});
        } else {
          res.json({success:true, reporte:reporte});
        }
      });
    } catch (e) {
      res.json({success:false});
    }
  });

  app.put("/apikmz/reporte/:id", function (req, res, next) {
    try {
      var id = req.params.id;
      Reporte.findById(id, function (err, reporte) {
        if (err) {
          res.json({success:false});
        } else {
          if (reporte) {
            if (req.body.listaPostes) {
              reporte.listaPostes = req.body.listaPostes;
            }
            reporte.empresa = req.body.empresa;
            reporte.instalacion = req.body.instalacion;
            reporte.ot = req.body.ot;
            reporte.nombreCliente = req.body.nombreCliente;
            reporte.direccion = req.body.direccion;
            reporte.codigoLargoMufaNodo = req.body.codigoLargoMufaNodo;
            reporte.idmufa = req.body.idmufa;
            reporte.codigoPuertoSplitter = req.body.codigoPuertoSplitter;
            reporte.secuenciaInicial = req.body.secuenciaInicial;
            reporte.secuenciaFinal = req.body.secuenciaFinal;
            reporte.hiloProcedencia = req.body.hiloProcedencia;
            reporte.hiloDestino = req.body.hiloDestino;

            reporte.habilitacion =     req.body.habilitacion;
            reporte.enrutado =         req.body.enrutado;
            reporte.tendido =          req.body.tendido;
            reporte.conectorizacion =  req.body.conectorizacion;
            reporte.factibilidadDeServicio = req.body.factibilidadDeServicio;
            reporte.postes =           req.body.postes;
            reporte.retiroFO =         req.body.retiroFO;
            reporte.ducto =            req.body.ducto;
            reporte.instalacionCT =    req.body.instalacionCT;
            reporte.traslado =         req.body.traslado;

            if (req.body.fechaInstalacion) {
              var finst = req.body.fechaInstalacion;
              finst = finst.split("/");
              finst =  new Date(finst[2], finst[1] -1, finst[0]);
              reporte.fechaInstalacion = finst;
            }

            if (req.body.fechainicio) {
            var fechainicio_ = req.body.fechainicio;
            fechainicio_ = fechainicio_.split("/");
            fechainicio_ =  new Date(fechainicio_[2], fechainicio_[1] -1, fechainicio_[0]);
            console.log(fechainicio_);
            reporte.fechainicio = fechainicio_;
            }

            if (req.body.fechafin) {
              var fechafin_ = req.body.fechafin;
              fechafin_ = fechafin_.split("/");
              fechafin_ =  new Date(fechafin_[2], fechafin_[1] -1, fechafin_[0]);
              console.log(fechafin_);
              reporte.fechafin = fechafin_;
            }
            console.log("antes de guardar: ", reporte);
            reporte.save(function (err) {
              if (err) {
                res.json({success:false});
              } else {
                res.json({success:true});
              }
            });
          } else {
            res.json({success:false});
          }
        }
      });
    } catch (e) {
      res.json({success:false});
    }
  });

  /*
  ** esta funcion debe devolver un conjunto de lineas xml
  ** necesarias para el doc.kml.
  ** devuelve texto en string.
  */
  function kmOpenStyleStr () {
    var prom = new Promise( function (resolve, reject) {
      fs.readFile('./img4kmz/objects/kmOpenStyle.xml',
      'utf8', function(err, data) {
        if (err) reject(err);
        //console.log(data);
        resolve(data);
      });
    });
    return prom;
  }


  // ATENCION ATENCION !!!
  // route :: /apikmz/download/:id
  // este endpoint fue sustituido por
  // app.get("/apikmz/download/:idreporte"
  // en el archivo restKMZkml.js
  app.get("borroesto/apikmz/download/:id", function (req, res, next) {
    var ide = req.params.id;
    Reporte.findById(ide, function (err, reporte) {
      if(err) { res.json({success:false});}
      else {
        var newFileName = './img4kmz/596e908236c5972b204dbaf7-YrDQQP/files/descontrolar.png';
        var fileName =   './img4kmz/downloads/'+ide+'.kmz'
        var fileOutput = fs.createWriteStream(fileName);
        var archive = archiver('zip');
        fileOutput.on('close', function () {
            console.log(archive.pointer() + ' total bytes');
            console.log('archiver has been finalized and the output file descriptor has closed.');
            res.json({success:true});
        });

        archive.pipe(fileOutput);
        archive.glob("img4kmz/596e908236c5972b204dbaf7-YrDQQP/**/*"); //some glob pattern here
        //archive.glob("../dist/.htaccess"); //another glob pattern
        // add as many as you like
        archive.on('error', function(err){
            res.json({success:false});
        });
        archive.finalize();

      }
    });

  });// fin -- route :: /apikmz/download/:id


/**
* Ruta:: /img4kmz
* Admite por post la recepcion de archivos,
* Los almacena en directorio_raiz/img4kmz
* Devuelve: el nombre del archivo ya subido
* La ruta casi completa en el disco duro del servidor.
*/
  app.post('/img4kmz', function(req, res, next){

    try {
    // create an incoming form object
    var form = new formidable.IncomingForm();
    // console.log("form",form);
    // console.log("body", req.body);

    // specify that we want to allow the user to upload multiple files in a single request
    form.multiples = true;

    // store all uploads in the /uploads directory
    //form.uploadDir = path.join(__dirname, '/img4kmz');

    var nameD = "";
    var nameD2 = new Date();
    var pathD = "";

    var dirchucha ="";
    form.parse(req, function (err, fields, files) {
      dirchucha = fields.chucha;
      console.log("dir enviado::: ", fields.chucha);
    });

    // store all uploads in the /img4kmz/::chucha directory
    form.uploadDir = path.join(__dirname, '/img4kmz');

    // every time a file has been uploaded successfully,
    // rename it to it's orignal name
    form.on('file', function(field, file) {
      pathD = file.path;
      //nameD = nameD2.getFullYear()+"-"+(nameD2.getMonth()+1)+"-"+nameD2.getDate()+"_"+file.name;
      nameD = file.name;
      fs.rename(file.path, path.join(form.uploadDir, file.name));
    });

    // log any errors that occur
    form.on('error', function(err) {
      console.log('An error has occured: \n' + err);
      res.json({success:false});
    });

    // once all the files have been uploaded, send a response to the client
    form.on('end', function() {
      //res.end('success');
      res.json({success:true, filename:nameD, pathD:pathD});
    });

    // parse the incoming request containing the form data
    //form.parse(req);
    } catch (e) {
      res.json({success:false});
    }

  }); // fin Ruta:: /img4kmz

  /**
  * Recibe por GET el filename que sera movido al directorio indicado
  */
  app.post("/img4kmz/move", function (req, res, next) {
    var filename_ = req.body.filename,
    directorio_ = req.body.directorio;
    var archivoPath = "./img4kmz/"+filename_;
    console.log(filename_, directorio_, archivoPath);

    if (fs.existsSync(archivoPath)) {
      if (!fs.existsSync("./"+directorio_+"/files")){
        fs.mkdirSync("./"+directorio_+"/files");
      }
      fs.rename(archivoPath,"./"+directorio_+"/files/"+filename_, function (err) {
        if (err) {
          res.json({success:false});
        } else {
          res.json({success:true, ruta:""+directorio_+"/files/"+filename_});
        }
      })
    } else {
      res.json({success:false});
    }
  }); // fin Ruta:: /img4kmz/move

};  // fin del componente a exportar
