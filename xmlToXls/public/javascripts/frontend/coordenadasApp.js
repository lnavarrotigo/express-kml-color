angular.module("coordenadasApp", [])
.controller("coordenadasCtrl", function ($scope, $http) {

    $scope.saludo = "La mesa del rincon";

    $scope.vpoints = [];
    $scope.textOutput = "";

    /** este método se encarga de separar la cadena de texto
    **  en donde vienen los pares de coordenadas por linea.
    **  cada coordenada viene en grados-minutos-segundos.
    **  resultado:
    **  $scope.vpoints es un arreglo de string, donde cada string
    **  contiene dos puntos (separados por coma) ej: -90 27 14.8,14 35 31.7
    **/
    /*$scope.metodo1 = function () {
        $scope.vpoints = [];
        $scope.textOutput ="";
        var x ="";
        for (i =0; i<$scope.textInput.length; i++) {
            x = $scope.textInput.charAt(i);
            switch (x) {
              case "°": break;
              case "'": break;
              case "\r": break; // return
              case "\n": $scope.textOutput += ";"; break;  // new line
              case "\"": break;
              case "\t":  $scope.textOutput += ",";  break;
              default: $scope.textOutput += x;  break;
            }
        } // fin del for

        // $scope.vpoints ->  es un arreglo de string, donde cada string contiene dos puntos (separados por coma) ej: -90 27 14.8,14 35 31.7
        $scope.vpoints = $scope.textOutput.split(";");$scope.metodo2();
        $scope.flagKml = true;
    };*/

    /** este método se encarga de separar la cadena de texto
    **  en donde vienen los pares de coordenadas por linea.
    **  cada coordenada viene en decimales.
    **  resultado:
    **  $scope.vpoints es un arreglo de string, donde cada string
    **  contiene dos puntos (separados por coma) ej: -90 27 14.8,14 35 31.7
    **/
    $scope.metodo1 = function () {
        $scope.vpoints = [];
        $scope.textOutput ="";
        var x ="";
        if ($scope.flagGrados && !$scope.flagDecimales) {
          for (i =0; i<$scope.textInput.length; i++) {
              x = $scope.textInput.charAt(i);
              switch (x) {
                case "°": break;
                case "'": break;
                case "\r": break; // return
                case "\n": $scope.textOutput += ";"; break;  // new line
                case "\"": break;
                case "\t":  $scope.textOutput += ",";  break;
                default: $scope.textOutput += x;  break;
              }
          } // fin del for


        }
        if ($scope.flagDecimales && !$scope.flagGrados) {
          for (i =0; i<$scope.textInput.length; i++) {
              x = $scope.textInput.charAt(i);
              switch (x) {
                case "°": break;
                case "'": break;
                case " ": break;
                case "\r": break; // return
                case "\n": $scope.textOutput += ";"; break;  // new line
                case "\"": break;
                default: $scope.textOutput += x;  break;
              }
          } // fin del for


        }
        // $scope.vpoints ->  es un arreglo de string, donde cada string contiene dos puntos (separados por coma) ej: -90 27 14.8,14 35 31.7
        $scope.vpoints = $scope.textOutput.split(";");
        $scope.metodo2();
        $scope.flagKml = true;
    };

    /*
    ** Toma al arreglo $scope.vpoints, para iterar sus elementos <string>,
    ** Se analizará cada par de puntos (grados-minutos-segundos) y se convertiran en puntos decimales.
    */
    $scope.metodo2 = function () {
      $scope.decimalPoints = [];
      var _pareja = [], _primero, _segundo, point1, point2;

      if ($scope.flagGrados && !$scope.flagDecimales) {
        for (var pareja in $scope.vpoints) {
          _pareja = $scope.vpoints[pareja];
          _pareja = _pareja.split(",");
          _primero = _pareja[0].split(" ");
          _segundo = _pareja[1].split(" ");
          console.log(_primero);
          console.log(_segundo);

          if (_primero[0].charAt(0) ==="-") {
            point1 = parseFloat(_primero[0]) + (parseFloat(_primero[1]) / -60) + (parseFloat(_primero[2]) / -3600);
          } else {
            point1 = parseFloat(_primero[0]) + (parseFloat(_primero[1]) / 60) + (parseFloat(_primero[2]) / 3600);
          }
          point2 = parseFloat(_segundo[0]) + (parseFloat(_segundo[1]) / 60) + (parseFloat(_segundo[2]) / 3600);
          $scope.decimalPoints.push({uno:point1, dos:point2});
          console.error(point1," -- ",point2);
        }
      }
      if ($scope.flagDecimales && !$scope.flagGrados) {
        for (var pareja in $scope.vpoints) {
          _pareja = $scope.vpoints[pareja];
          _pareja = _pareja.split(",");
          _primero = _pareja[0];
          _segundo = _pareja[1];
          console.log(_primero);
          console.log(_segundo);
          $scope.decimalPoints.push({uno:_primero, dos:_segundo});
          console.error(_primero," -- ",_segundo);
        }
      }

      if ($scope.decimalPoints.length >0) {
        $scope.showGenerateKml = true;
      }

    };

    $scope.metodo3 = function () {
        //rest.js
        $http.post("/kml-points", {points:$scope.decimalPoints})
        .then( function(res) {
          if (res.data.success) {
            $scope.showDownloadKml = true;
            if ( res.data.filename) $scope.filename = res.data.filename;
          }
        });
    };

    $scope.useDecimales = function () {
      $scope.flagDecimales = true;
      $scope.flagGrados = false;
    };

    $scope.useGrados = function () {
      $scope.flagDecimales = false;
      $scope.flagGrados = true;
    };


    $scope.xlsxgenerated;
    $scope.messages = [];
    $scope.gif = false;
    $scope.imgicon = "http://maps.google.com/mapfiles/kml/shapes/open-diamond.png";
    /*
    message = {
      title:"", text: "", classtype:""
    }
    */
    $scope.imgnumber = "0";
    $scope.setIcon = function () {
      switch ($scope.imgnumber) {
        case "0":
          $scope.imgicon = "http://maps.google.com/mapfiles/kml/shapes/open-diamond.png";
          break;
        case "1":
          $scope.imgicon = "http://maps.google.com/mapfiles/kml/shapes/square.png";
          break;
        case "2":
          $scope.imgicon = "http://maps.google.com/mapfiles/kml/shapes/ranger_station.png";
          break;
        case "3":
          $scope.imgicon = "http://maps.google.com/mapfiles/kml/shapes/donut.png";
          break;
        case "4":
          $scope.imgicon = "http://maps.google.com/mapfiles/kml/shapes/placemark_square.png";
          break;
        case "5":
          $scope.imgicon = "http://maps.google.com/mapfiles/kml/shapes/placemark_circle.png";
          break;
        default:

      }

    };

    $scope.chk_ruta = "NO";
    $scope.get_download_link = function () {
      var xlsxname = $("#hiddenfilename").val();
      console.log("vamos a procesar _::"+ xlsxname+"::_");
      $scope.gif = true;
      $("#getdownloadlink").hide(300);
      // este endpoint esta en rest.js
      // alert($scope.chk_ruta);
      $http.post("/leer-xlsx-coordenadas", {filename:xlsxname, iconurl:$scope.imgicon, ruta:$scope.chk_ruta})
      .then( function (res) {
          if (res.data.success) {
            $scope.gif = false;
            $scope.xlsxgenerated = res.data.filename;
            $scope.messages.push({
              title:"info", classtype:"", text:"Procesamiento exitoso!"
            });
          }
        }, function (error) {
          $scope.messages.push({
            title:"Error", classtype:"", text:"Sucedió un error!"
        });
      });
    };


}); //fin del controller
