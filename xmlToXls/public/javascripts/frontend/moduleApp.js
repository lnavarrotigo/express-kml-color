var moduleApp = angular.module("moduleApp", []);

moduleApp.controller("CtrlMain", function ($scope, $http) {
  var app = this;
  app.mensaje ="angular funcionando!";
  app.alertVerDatos = false;
  app.fileList2 = ["hola","que","tal"];

  app.estado1 ="";
  app.estado2="";
  console.log(app.mensaje);

  $scope.enviado = false;

  app.refresh = function () {
    //$http.get("/borrar/kml-lectura")
    $http.get("/api/basic")
    .then( function (res) {
      console.log(res.data);
    });


  }; //app.refresh();

  app.getFileList = function () {
      console.log("vamos a obtener las chivas");
      $http.get("/getFileList")
      .then( function (res) {
          if (res.data.fileList) {
            app.fileList = res.data.fileList;
            console.log("vino asi", app.fileList);
          } else {
            console.log("no vino lista de archivos", res.data);
          }
      });
  };

  app.fAlertVerDatos = function () {
    app.alertVerDatos = true;
  };

  // se manda a traer el listado de nuevos colores para pintar cada municipio.
  app.procesarDatos = function () {
      if (app.selectedFile) {
        console.log("se analizara el archivo: ", app.selectedFile);
        $http.post('/leer-xlsx', {filename:app.selectedFile})
        .then( function (res) {
          if (res.data.success) {
            console.log("Excel cargado ");
            app.estado1 = "Excel cargado.";

                $http.get('/generarKML')
                .then( function (res) {
                  if (res.data.success) {
                    app.estado2 = "Archivo Generado"
                  }
                });
          } else {
            console.log("No se recibio la respuesta con exito.")
          }
        });
      }
  };

  app.generarKML = function () {
      $http.get('/generarKML')
      .then( function (res) {

      });
  };

});
