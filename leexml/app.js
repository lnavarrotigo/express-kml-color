var fs = require('fs');
    //xml2js = require('xml2js');

var placemark_impar =
				{
					"@":{"id":"kml_1"},
					"name":"HUITE",
					"Style" : {
						"IconStyle" : {
							"scale" : "0.8",
							"Icon" : {
								"href" : "http://maps.google.com/mapfiles/kml/shapes/placemark_circle.png"
							}
						},
						"LabelStyle" : {
							"scale" : "1.0"
						},
						"LineStyle" : {
							"color" : "",
							"width" : "2"
						},
						"PolyStyle" : {
							"color" : "3300aaff"
						}
					},
					"Point" : {
						"coordinates" : "-89.7000732421875,14.9097852706909,0"
					}
				};
        var placemark_par =
                {
                  "@" : {"id":"kml_2"},
                  "name":"HUITE",
                  "visibility" : "1",
                  "Style" : {
                    "IconStyle" : {
                      "scale" : "0.8",
                      "Icon" : {
                        "href" : "http://maps.google.com/mapfiles/kml/shapes/placemark_circle.png"
                      }
                    },
                    "LabelStyle" : {
                      "scale" : "1.0"
                    },
                    "LineStyle" : {
                      "color" : "",
                      "width" : "2"
                    },
                    "PolyStyle" : {
                      "color" : "3300aaff"
                    }
                  },
                  "Polygon" : {
                    "extrude" : "0",
                    "tessellate" : "1",
                    "outerBoundaryIs" : {
                      "LinearRing" : {
                        "coordinates" : "-90.8642842311222,14.8083714884016,0 -90.8695809087851,14.8118451500473,0 -90.8774925410162,14.8106531775224,0 -90.8803886719318,14.8096960684928,0 -90.8859655378648,14.8056822526161,0 -90.8899936164185,14.8033359413619,0 "
                      }
                    }
                  }
                };
/*var parser = new xml2js.Parser();
fs.readFile(__dirname + '/prueba.kml', function(err, data) {
    parser.parseString(data, function (err, result) {
        console.dir(result);
        console.log('Done');
    });
});*/
var js2xmlparser = require("js2xmlparser");

var mun = js2xmlparser.parse("Placemark", placemark_par);
mun = mun.replace("<?xml version='1.0'?>","");
console.log(mun);
