angular.module("reportepostesApp", [])

.controller("reportepostesController", function ($scope, $http) {
  $scope.rows = {
    okrows :[], noidrows:[]
  };
  $scope.init = function () {
    // almacena el numero que indica en que paso va el proceso,
    // para mostrar en la vista, el componente respectivo.
    $scope.paso = 1;
    // almacena el nombre del archivo que se subio y se guardo en el server
    $scope.archivoEnServidor = "";
    // $scope.tipoPostes Almacena el nombre de la unidad de negocio para la que
    // se aplica el registro de postes en esta sesion (archivo xlsx)
    $scope.tipoPostes = "";

    $scope.flagDatosActualizados = false;

    $scope.alertDivs = [];

    $scope.rows = {
      okrows :[], noidrows:[]
    };
  };
  $scope.init();

  $scope.doPaso = function (paso) {
    $scope.paso = paso;
    switch (paso) {
      // en este paso, estamos pidiendo al servidor que procese el archivo mencionado en "archivoEnServidor"
      // que ya ha sido subido.
      // return: res.data.okrows > la lista de filas que se encontraron en base de datos
      // return: res.data.noidrows > la lista de filas que no se encontraron en base de datos.
      case 2:
        // obtener el nombre del archivo creado en el servidor
        var archivoEnServidor = angular.element( document.querySelector( '#existe-archivo' ) );
        $scope.archivoEnServidor = archivoEnServidor.text();
        console.log(archivoEnServidor);

        // pedir al endpoint que lea la cantidad de filas en el archivo xlsx indicado
        $http.get("/add-poste-from-xlsx/"+archivoEnServidor.text())
        .then( function(res) {
          if (res.data.success) {
            $scope.rows.okrows = res.data.okrows;
            $scope.rows.noidrows = res.data.noidrows;

            // recorremos la lista de filas que se encontraron en base de datos,
            // para crear una nueva columna, en la que se sumen los cables que
            // contiene cada poste.
            $scope.rows.okrows.forEach(function(elem, i, arr) {
              $scope.rows.okrows[i]["sumado"] = parseInt($scope.rows.okrows[i].row[7]) + $scope.rows.okrows[i].cables;
            });
            $scope.setAlert('alert-warning', 'Revisiones al archivo xlsx', 'Verifíque los listados a continuación.');
            console.log('xlsx procesado',res.data);
          }
        });
        break;

      // en este paso se envia una lista al servidor, cuyos elementos son registros existentes en base de datos,
      // correspondientes a postes existentes que según el archivo de excel subido, tienen que aumentar los cables
      // registrados en el citado poste.
      // tambien se debe enviar el nombre del archivo (archivoEnServidor) y la lista de postes (rows.noidrows)
      case 3:
        if ($scope.archivoEnServidor && $scope.tipoPostes && $scope.rows && $scope.rows.okrows) {
          //$scope.setAlert("alert-info","proceso","vamos a enviar a actualizar");
          var misdatos = {};
          misdatos.nombreArchivo = $scope.archivoEnServidor;
          misdatos.tipoPostes = $scope.tipoPostes;
          misdatos.lista = $scope.rows.okrows;
          $http.post("/update-postes-from-list", misdatos)
          .then( function (res) {
            if (res.data.success) {
              $scope.flagDatosActualizados = true;
              console.log("datos actualizados",res.data.datos);
            }
          });
          $scope.paso=2;
        }
        break;

      // en este paso se envia una lista al servidor, cuyos elementos son registros a crear en la base de datos,
      // correspondientes a postes que según el archivo de excel subido, tienen que cables que suman para la
      // respectiva unidad de negocio (home, BUC, etc.)
      // tambien se debe enviar el nombre del archivo (archivoEnServidor) y la lista de postes (rows.noidrows)
      case 4:
        if ($scope.archivoEnServidor && $scope.tipoPostes && $scope.rows && $scope.rows.noidrows) {

          var datosCreate = {};
          datosCreate.nombreArchivo = $scope.archivoEnServidor;
          datosCreate.tipoPostes = $scope.tipoPostes;
          datosCreate.lista = $scope.rows.noidrows;
          $http.post("/create-postes-from-list", datosCreate)
          .then( function (res) {
            if (res.data.success) {
              $scope.flagDatosCreados = true;
              console.log("datos creados",res.data.datos);
            }
          });
          $scope.paso=2;
        }
        break;

      default:

    } // fin switch
  }; // fin doPaso()

  $scope.downloadXlsxPostes = function () {
    $scope.load_listapostes = true;
    $http.get("/filedata-postes")
    .then( function (res) {
      if (res.data.success) {
        $scope.load_listapostes = false;
        var linkE = document.createElement('a');
        linkE.setAttribute('href', '../salida.xlsx');
        linkE.click();
      }
    })
  };
  $scope.stat = {};
  $scope.getStat = function () {
    $scope.load_stat = true;
    $http.get("/statistics").then(function (res) {
      if (res.data.success) {
        $scope.load_stat=false;
        console.log(res.data.unos);
        $scope.stat = {
          home : res.data.unos.unoshome/(res.data.unos.unoshome+res.data.unos.unosnavega+res.data.unos.unosmobile),
          navega : res.data.unos.unosnavega/(res.data.unos.unoshome+res.data.unos.unosnavega+res.data.unos.unosmobile),
          mobile : res.data.unos.unosmobile/(res.data.unos.unoshome+res.data.unos.unosnavega+res.data.unos.unosmobile),
          total : res.data.unos.unoshome+res.data.unos.unosnavega+res.data.unos.unosmobile,
          home_ : res.data.unos.unoshome,
          navega_ : res.data.unos.unosnavega,
          mobile_ : res.data.unos.unosmobile,

          trelec : res.data.unos.trelec,
          eegsa : res.data.unos.eegsa,
          cables1 : res.data.unos.cables1,
          cablesmas : res.data.unos.cablesmas,
          total : res.data.unos.total
        };
      }
    });
  };

  $scope.setAlert = function (clase, titulo, mensaje) {
    $scope.alertDivs.push({class:clase, title:titulo, mensaje:mensaje});
    var positionAlert = $(".alert").offset();
    if (positionAlert && positionAlert.top) {
      $('html, body').animate({
          scrollTop: positionAlert.top
        },
        1300
      );
    } else {
      $('html, body').animate({
          scrollTop: 0
        },
        1300
      );
    }

  };

  $scope.deleteAlert = function (elem) {
    $('.alert').fadeOut(1500);
    var index = $scope.alertDivs.indexOf(elem);
    $scope.alertDivs.splice(index, 1);
  }

})

.filter('percentage', ['$filter', function ($filter) {
  return function (input, decimals) {
    return $filter('number')(input * 100, decimals) + '%';
  };
}]);
