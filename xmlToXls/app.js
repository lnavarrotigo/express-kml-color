var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var index = require('./routes/index');
var users = require('./routes/users');
var cambiacolor = require('./routes/cambiacolor');
var coordenadas = require('./routes/coordenadas');
var estable = require('./routes/estable');
var rutas = require('./routes/rutas');
var reporteKML = require('./routes/reporteKML');
var reportePostes = require('./routes/postes/reportePostes');
var listaPostes = require('./routes/postes/lista');
var estadisticas = require('./routes/postes/estadisticas');

var json2csv = require('nice-json2csv');
var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'excelReports')));
app.use(express.static(path.join(__dirname, '/excelReports/formato_postes.xlsx')));
app.use(express.static(path.join(__dirname, 'img4kmz/downloads')));
app.use(express.static(path.join(__dirname, 'fileCoordenadas/result')));
app.use(express.static(path.join(__dirname, 'fileCoordenadas/PLANTILLA')));
app.use(express.static(path.join(__dirname, 'resultado.kml')));
app.use(json2csv.expressDecorator);


// cambiacolor y estable en teoria, son lo mismo
// resulta que cuando un cambio (se aplica en cambiacolor) se termina y probado funciona,
// entonces ese desarrollo se pasa a la vista estable
app.use('/', index);
app.use('/users', users);
app.use('/cambiacolor', cambiacolor);
app.use('/coordenadas', coordenadas);
app.use('/rutas', rutas);
app.use('/reporteKML', reporteKML);
app.use('/admin/reportePostes', reportePostes);
app.use('/admin/listaPostes', listaPostes);
app.use('/admin/estadisticas', estadisticas);

app.post('/uuu', function (req, res, next) {
  fs.unlink(req.body.file2, function(err) {
    if (err) console.log(err);
    console.log('file successfully deleted');
  });
});
var rest       = require('./rest')(app);
var restKMZ    = require('./restKMZ')(app);
var restPostes = require('./restPostes')(app);
var restKMZaux = require('./restKMZaux')(app);
var restKMZkml = require('./restKMZkml')(app);
// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
