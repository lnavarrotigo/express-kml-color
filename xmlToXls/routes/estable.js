var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('estable', { title: 'Cambia color a municipios' });
});

module.exports = router;
