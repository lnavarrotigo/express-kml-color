var path = require('path');
var formidable = require('formidable');
var fs = require('fs');

// para manejar archivos excel
var parseXlsx = require('excel');

// para manejar data de mongo hacia archivos xlsx
var mongoXlsx = require('mongo-xlsx');

module.exports = function (app) {

  var Poste = require("./models").posteModel;

  /*
  *  ruta:: /file-xlsx-postes-to-database/:filename
  *  este endpoint se encarga de recibir el nombre de un archivo .xlsx,
  *  el cual va a ser procesado de la siguiente forma:
  *  las filas de su hoja 1 representan un poste, que sera insertado a la base de datos.
  *  Esta ruta servira para cargar nueva data a la base de datos, no sobreescribira datos,
  *  siempre va a crear un nuevo registro para cada fila encontrada.
  */
  app.get('/file-xlsx-postes-to-database/:filename/:pestana', function (req, res, next) {
    var _filename = req.params.filename;
    var _pestana = req.params.pestana;
    var resultado = false;
    //var objPoste = {};
    // evalua si existe en la ruta de entrada, el nombre de un archivo
    if (_filename && _pestana) {
      console.log("Vino :filename  :: ", _filename);
      parseXlsx('filePostesExcel/'+_filename, _pestana, function(err, data) {
        resultado = true;
        console.log(resultado);
      	if(!err) {
          console.log("filePostesExcel/"+_filename);
          var antes = new Date();
          // data is an array of arrays
          data.forEach( function (elem, i, arr) {
            if (i>0) {
              var objPoste = new Poste({
                idposte : elem[0],
                descripcion : elem[1],
                propiedad : elem[2],
                departamento : elem[3],
                fechaInstFibra : elem[4],
                cables : elem[5],
                mes : elem[6],
                alumbrado : elem[7],
                navega : elem[9],
                home : elem[10],
                mobile : elem[11],
              });

              objPoste.save(function (err) {
                if (err) {
                  console.log(i," ha producido un error.\n");
                }
              });
              console.log(i," ha terminado .save \n");
            }
          });
          var despues = new Date();
          var tiempo =  (despues.getTime() - antes.getTime() )/1000;
          console.log("se tardo ",tiempo," segundos.");
          res.json({success:resultado, segundos:tiempo});
        } else {
          res.json({success:false, message:"Error.", error: err});
        }
      });
    } else {
      res.json({success:resultado, message:"No indico el nombre de un archivo."});
    }


  });

  /**
  * ruta:: /file-postes-excel
  * Este endpoint sirve para procesar por POST, la recepcion de un archivo xlsx
  * subido desde un formulario.
  * Al recibir el archivo, le cambia el nombre y lo guarda en el directorio
  * ./filePostesExcel . Al finalizar exitosamente, devuelve un nombre del archivo creado
  * y guardado como año-mes-dia_nombre
  */
  app.post('/file-postes-excel', function(req, res, next){

    try {
    // create an incoming form object
    var form = new formidable.IncomingForm();

    // specify that we want to allow the user to upload multiple files in a single request
    form.multiples = true;

    // store all uploads in the /uploads directory
    form.uploadDir = path.join(__dirname, '/filePostesExcel');

    var nameD = "";
    var nameD2 = new Date();
    var pathD = "";

    // every time a file has been uploaded successfully,
    // rename it to it's orignal name
    form.on('file', function(field, file) {
      pathD = file.path;
      nameD = nameD2.getFullYear()+"-"+(nameD2.getMonth()+1)+"-"+nameD2.getDate()+"_"+file.name;
      fs.rename(file.path, path.join(form.uploadDir, nameD));
    });

    // log any errors that occur
    form.on('error', function(err) {
      console.log('An error has occured: \n' + err);
    });

    // once all the files have been uploaded, send a response to the client
    form.on('end', function() {
      //res.end('success');
      res.json({success:true, filename:nameD, pathD:pathD});
    });

    // parse the incoming request containing the form data
    form.parse(req);
    } catch (e) {
      res.json({success:false});
    }

  });

  /**
  * ruta:: /add-poste-from-xlsx
  * Este endpoint se encarga de leer un archivo que esta ubicado en el directorio
  * ./filePostesExcel/año-mes-dia_<filename>.
  * En dónde filename, es un string que llego como parametro por la url.
  * Al existir el archivo, lo lee y carga todas las filas y comprueba que la informacion
  * coincida con la base de datos.
  * Separa en <okrows> y <noidrows>, los datos que encuentre en la DB según sea el caso.
  */
  app.get('/add-poste-from-xlsx/:filename', function (req, res, next) {
    var _filename = 'filePostesExcel/'+req.params.filename;
    var failedrows = [];
    var noidrows = [];
    var okrows = [];
    function fillokrows(obj) {
      okrows.push(obj);
    }
    function fillnoidrows(obj) {
      noidrows.push(obj);
    }
    parseXlsx(_filename, function(err, data) {

      data.forEach( function (elem, i, arr) {
        if (i>0) {
          /*var objPoste = new Poste({
            idposte : elem[0],
            descripcion : elem[1],
            propiedad : elem[2],
            departamento : elem[3],
            fechaInstFibra : elem[4],
            cables : elem[5],
            mes : elem[6],
            alumbrado : elem[7],
            navega : elem[9],
            home : elem[10],
            mobile : elem[11],
          });*/
          Poste.find({idposte:elem[0]}, function (err, postes) {
            if(!err) {
                if (postes && postes.length >0) {
                  //console.log(postes[0]);
                  //okrows.push();
                  fillokrows({i:i, row:elem, id_:postes[0]["_id"], cables:postes[0]["cables"]});
                  if ( (i+1)==data.length ) {
                    console.log('- - - - - - - - -- - - - - - -');
                    console.log(okrows);
                    console.log(noidrows);
                    res.json({success:true, okrows:okrows, noidrows:noidrows});
                  }
                } else {
                  //noidrows.push();
                  fillnoidrows({i:i, row:elem});
                  if ( (i+1)==data.length ) {
                    console.log('- - - - - - - - -- - - - - - -');
                    console.log(okrows);
                    console.log(noidrows);
                    res.json({success:true, okrows:okrows, noidrows:noidrows});
                  }
                }
            }
          });

        }

      });

    }); // fin del parseXlsx


  }); // fin de :::app.get('/add-poste-from-xlsx/:filename

  /**
  * Este endpoint se encarga de CREAR a partir de una lista de postes
  * que será necesario verbo en la base de datos.
  */
  app.post("/create-postes-from-list", function (req, res, next) {
    var dat = req.body;
    var listaErrores = [];
    dat.lista.forEach( function (elem, i, arr) {

      var nuevoPoste = new Poste ({
        idposte : elem.row[0],
        descripcion : elem.row[1],
        propiedad : elem.row[2],
        departamento : elem.row[3],
        fechaInstFibra : elem.row[4],
        cables : elem.row[7],
        mes : elem.row[6]
      });

      switch (dat.tipoPostes.toUpperCase()) {
        case 'HOME':
          nuevoPoste.home = '1';
          break;
        case 'NAVEGA':
          nuevoPoste.navega='1';
          break;
        case 'MOBILE':
          nuevoPoste.mobile = '1';
          break;
        case 'MOVILE':
          nuevoPoste.mobile = '1';
          break;
        default:
      }

      nuevoPoste.save(function (err) {
        if (err) {
          listaErrores.push({fila:elem.i, idposte: elem.row[0]});
        } else {
          if ( (i+1)==dat.lista.length) {
            res.json({success:true, listaerrores : listaErrores});
          }
        }
      });

    }); // fin forEach

  });
  /**
  * Este endpoint se encarga de ACTUALIZAR a partir de una lista de postes
  * que será necesario verbo en la base de datos.
  */
  app.post("/update-postes-from-list", function (req, res, next) {
    var dat = req.body;
    var actualizados = [];
    //nombreArchivo
    //tipoPostes
    //
    dat.lista.forEach( function (elem, i, arr) {
        Poste.findOneAndUpdate(
          {_id:elem.id_, idposte:elem.row[0]},
          { $set: {
              cables:elem.sumado
            }
          },
          {new: true},
          function (err, poste) {
            if (!err) {
              switch (dat.tipoPostes.toUpperCase()) {
                case 'HOME':
                  poste.home = '1';
                  break;
                case 'NAVEGA':
                  poste.navega='1';
                  break;
                case 'MOBILE':
                  poste.mobile = '1';
                  break;
                default:
              }
              poste.save(function (err) {
                if (!err) {
                  actualizados.push(poste);
                  if ( (i+1)==dat.lista.length) {
                    res.json({success:true, datos:actualizados});
                  }
                } else {
                  res.json({success:false});
                }
              });
            } else {
              res.json({success:false});
            }
        });
    });
  });

  /**
  * ruta:: /update-postes-encontrados
  * Este endpoint se encarga de recibir una lista de registros existentes en la DB,
  * y procede a actualizar la informacion que desde el cliente ya ha sido confirmada
  * por el usuario.
  */
  app.post("/update-postes-encontrados", function (req, res, next) {
    var archivoEnServidor = req.body.archivoEnServidor,
        rows = req.body.row

  }); // fin ruta :: /update-postes-encontrados

  /**
  * ruta :: /filedata-postes
  * endpoint para solicitar la descarga de un archivo xlsx con datos provenientes de mongodb.
  * Se trata de ejecutar un metodo que convierta el result-query de mongodb a un archivo xlsx,
  * al terminar el metodo, devuelve un objeto json, el cual contiene la info de la operacion
  * que termino exitosa.
  * Al cliente se le devuelve un json {success:true}, el cual le permite crear con javascript
  * un elemento a link, al que se le agrega la propiedad href y seguido se invoca al metodo
  * .click(), para que empiece a descargar automáticamente.
  */
  app.get("/filedata-postes", function (req, res, next) {
    Poste.find({},{__v:0, _id:0}, function (err, postes) {
      if (!err) {
        var model = mongoXlsx.buildDynamicModel(postes);
        /* Generate Excel */
        mongoXlsx.mongoData2Xlsx(postes, model,
          { path:'excelReports', fileName:'salida.xlsx', sheetName: ['PoStEs']},
          function(err, data) {
            if (err) {
              res.json({success:false});
            } else {
              console.log('File saved at:', data.fullPath);
              res.json({success:true});
            }
        });
      }
    });
  }); // fin ruta :: /filedata-postes

  app.get("/statistics", function (req, res, next) {
    var obj = {};
    Poste.count({$where: "this.idposte.length = 6", navega:'1'}, function (err, c) {
      if (!err) {
        obj.unosnavega = c;
        Poste.count({$where: "this.idposte.length = 6", home:'1'}, function (err, c) {
          if (!err) {
            obj.unoshome = c;
            Poste.count({$where: "this.idposte.length = 6", mobile:'1'}, function (err, c) {
              if (!err) {
                obj.unosmobile = c;
                Poste.count({propiedad:'TRELEC'}, function (err, c) {
                  if (!err) {
                    obj.trelec = c;
                    Poste.count({propiedad:'EEGSA'}, function (err, c) {
                      if (!err) {
                        obj.eegsa = c;
                        Poste.count({cables:1}, function (err, c) {
                          if (!err) {
                            obj.cables1 = c;
                            Poste.count({cables:{$gt:1} }, function (err, c) {
                              if (!err) {
                                obj.cablesmas = c;
                                Poste.count({}, function (err, c) {
                                  if (!err) {
                                    obj.total = c;
                                    res.json({success:true, unos:obj});
                                  }
                                });
                              }
                            });
                          }
                        });
                      }
                    });
                  }
                });
              }
            });
          }
        });
      }
    });
  });




};  // fin del componente a exportar
