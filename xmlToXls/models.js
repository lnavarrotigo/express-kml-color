var mongoose = require('mongoose');
var Schema = mongoose.Schema;
mongoose.connect('mongodb://localhost/kmldb');

var municipioSchema = new Schema({
  codigo: { type: String},
  nombre: { type: String},
   depto: { type: String},
  tiempo: { type: String},
   rango: { type: String},
  numImpar: { type: String},
  point:  { type: String},
  numPar: { type: String},
  linearRing : { type: String}
});

module.exports.municipioModel = mongoose.model('municipio', municipioSchema);

var reporteSchema = new Schema({
  empresa : { type: String},
  instalacion : { type: String},
  ot : { type: String},
  nombreCliente : { type: String},
  direccion : { type: String},
  codigoLargoMufaNodo : { type: String},
  idmufa : { type: String},
  codigoPuertoSplitter : { type: String},
  fechaInstalacion : { type: Date},
  secuenciaInicial : { type: String},
  secuenciaFinal : { type: String},
  hiloProcedencia : { type: String},
  hiloDestino : { type: String},

  habilitacion : { type: Boolean },
  enrutado: { type: Boolean },
  tendido: { type: Boolean },
  conectorizacion: { type: Boolean },
  factibilidadDeServicio: { type: Boolean },
  postes: { type: Boolean },
  retiroFO: { type: Boolean },
  ducto: { type: Boolean },
  instalacionCT: { type: Boolean },
  traslado: { type: Boolean },

  fechainicio : { type: Date },
  fechafin : { type: Date },
  listaPostes : [],

  folder: { type:String },
  kml: { type:String }
});

module.exports.reporteModel = mongoose.model('reporte', reporteSchema);

var posteSchema = new Schema({
  idposte : { type: String},
  descripcion : { type: String},
  propiedad : { type: String},
  departamento : { type: String},
  fechaInstFibra : { type: String},
  cables : { type: Number},
  mes : { type: String},
  alumbrado : { type: String},
  navega : { type: String},
  home : { type: String},
  mobile : { type: String},
});
module.exports.posteModel = mongoose.model('poste', posteSchema);
