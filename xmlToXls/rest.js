module.exports = function (app) {
    /* @throws TypeError: Converting circular structure to JSON
     No copia funciones ni objetos con estructuras circulares.
     Debe respetar estrictamente los valores permitidos por JSON.
     Lea la especificación del formato JSON: http://json.org/
    */

    function CLONE (obj) {
      return JSON.parse(JSON.stringify(obj));
    }

    /**
    * @cadena es el texto que se desea utilizar sin caracteres "extraños".
    */
    function getCleanedString(cadena){
     // Definimos los caracteres que queremos eliminar
     var specialChars = "!@#$^&%*()+=-[]\/{}|:<>?,.";

     // Los eliminamos todos
     for (var i = 0; i < specialChars.length; i++) {
         cadena= cadena.replace(new RegExp("\\" + specialChars[i], 'gi'), '');
     }

     // Lo queremos devolver limpio en minusculas
     cadena = cadena.toLowerCase();

     // Quitamos espacios y los sustituimos por _ porque nos gusta mas asi
     cadena = cadena.replace(/ /g,"_");

     // Quitamos acentos y "ñ". Fijate en que va sin comillas el primer parametro
     cadena = cadena.replace(/á/gi,"a");
     cadena = cadena.replace(/é/gi,"e");
     cadena = cadena.replace(/í/gi,"i");
     cadena = cadena.replace(/ó/gi,"o");
     cadena = cadena.replace(/ú/gi,"u");
     cadena = cadena.replace(/ñ/gi,"n");
     cadena = cadena.trim();
     return cadena;
  }
  // JSON to CSV Converter
  function ConvertToCSV(objArray) {
      var array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;
      var str = '';

      for (var i = 0; i < array.length; i++) {
          var line = '';
          for (var index in array[i]) {
              if (line != '') line += ','

              line += array[i][index];
          }

          str += line + '\r\n';
      }

      return str;
  }

  var Municipio = require("./models").municipioModel;
  /*var mixco = new Municipio({
      codigo: "10001",
      nombre: "Mixco",
       depto: "Guatemala",
      tiempo: "5",
       rango: "AZUL",
      numero: "57"
  });
  mixco.save();*/

  var fs = require('fs'),
      xml2js = require('xml2js');
  var json2csv = require('json2csv');
  var Js2Xml = require("js2xml").Js2Xml;
  app.get('/api/basic', function (req, res, next) {
    var result2 = 0;
    var parser = new xml2js.Parser();
    var fields = ['col0', 'col1', 'col2', 'col3', 'col4'];
    var hoja = [];
    fs.readFile(__dirname + '/solo3.kml', function(err, data) {
        parser.parseString(data, function (err, result) {
            if (err) {
              res.json({error:err});
            }
            //res.json(result.kml.Document[0].Folder[0].Placemark);

            if (result.kml.Document[0].Folder[0].Placemark) {
                //console.log(result.Document.Placemark);
                var count = 0;
                var filita = {};
                result.kml.Document[0].Folder[0].Placemark.forEach( function (elem) {
                  count++;
                  //console.log(elem.$.id);
                  if (count===1) {
                    filita["col0"] = elem.name[0];
                    console.log(elem.name[0]);
                    filita["col1"] = elem.$.id;
                    filita["col2"] = elem.Point[0].coordinates[0];

                    // update on db
                    var nombreMuni = elem.name[0].toLowerCase().trim();
                    nombreMuni = getCleanedString(nombreMuni);
                    var d = [];
                    Municipio.find({}, function (err, municipios) {
                      if (err) {
                        res.json({success:false, error:err});
                      } else {
                        municipios.forEach( function (el) {
                          var dbmuni = getCleanedString(el.nombre.toLowerCase().trim() );
                          if (dbmuni==nombreMuni ){
                            el.numImpar = elem.$.id;
                            el.point = elem.Point[0].coordinates[0];
                            el.save();
                          }
                        });
                      }
                    });
                    // fin update on db
                  } else if (count === 2) {
                    filita["col3"] = elem.$.id;
                    filita["col4"] = elem.Polygon[0].outerBoundaryIs[0].LinearRing[0].coordinates[0];

                    // update on db
                    var nombreMuni = elem.name[0].toLowerCase().trim();
                    nombreMuni = getCleanedString(nombreMuni);
                    var d = [];
                    Municipio.find({}, function (err, municipios) {
                      if (err) {
                        res.json({success:false, error:err});
                      } else {
                        municipios.forEach( function (el) {
                          var dbmuni = getCleanedString(el.nombre.toLowerCase().trim() );
                          if (dbmuni==nombreMuni ){
                            el.numPar = elem.$.id;
                            el.linearRing = elem.Polygon[0].outerBoundaryIs[0].LinearRing[0].coordinates[0];
                            el.save();
                          }
                        });
                      }
                    });
                    // fin update on db

                    // hacer varias cosas,
                    hoja.push(filita);
                    filita = {};
                    count = 0;
                  }
                });
                //console.log(hoja);
                //res.csv(hoja, "myFile.csv");
                res.json({placemark:hoja});

            }


            //res.json({csv:result2});
            //console.log(result);
            //console.log('Done');
        });
    });
    /*var hojaJSON = JSON.stringify(hoja);
    var csvContent = json2csv({ data: hoja, fields: fields });
    fs.writeFile('municipiooooooooooooooos.csv', csvContent, function(err) {
        if (err) throw err;
        console.log('file saved');
    });*/


  });

  // using togeojson for read kml file and get a geojson object.
  var tj = require('togeojson'),
      // node doesn't have xml parsing or a dom. use xmldom
      DOMParser = require('xmldom').DOMParser;
  var tokml = require('tokml');
  app.get('/borrar/kml-lectura', function (req, res, next) {
    // leemos el kml y lo convertimos a geojson
    var kml = new DOMParser().parseFromString(fs.readFileSync('solo3.kml', 'utf8'));
    var convertedWithStyles = tj.kml(kml, { styles: true });
    var count = 0;
    for (var municipio in convertedWithStyles.features) {
      count++;
      if (count === 1) {
        convertedWithStyles.features[municipio].properties.fill = "red";
        convertedWithStyles.features[municipio].properties.stroke = "red";
      } else if (count === 2) {
        convertedWithStyles.features[municipio].properties.fill = "red";
        convertedWithStyles.features[municipio].properties.stroke = "red";

        count = 0;
      }

    }

    // ahora convertimos el geojson a una cadena con sintaxi kml (luego sera guardada como archivo .kml)
    // kml_converted is a string of KML data, geojsonObject is a JavaScript object of
    // GeoJSON data
    var kml_converted = tokml(convertedWithStyles);

    // escritura del archivo de salida
    var fs2 = require('fs');
    fs2.writeFile('exitoooooo.kml', kml_converted, function (err) {
      if (err) return console.log(err);
      console.log('Hello World > helloworld.txt');
    });
    // termina la escritura del archivo de salida
    res.json(convertedWithStyles);
  });

// lineas para subir archivos al servidor
var multer = require('multer');

var storage =   multer.diskStorage({
  destination: function (req, file, callback) {
    callback(null, './uploads');
  },
  filename: function (req, file, callback) {
    callback(null, file.originalname);
  }
});
var storage2 =   multer.diskStorage({
  destination: function (req, file, callback) {
    callback(null, './fileCoordenadas');
  },
  filename: function (req, file, callback) {
    callback(null, file.originalname);
  }
});


var  upload = multer({ storage :  storage}).single('myfile');
var upload2 = multer({ storage : storage2}).single('myfile');


app.post('/uploadjavatpoint',function(req,res){
    upload(req,res,function(err) {
        if(err) {
            return res.end("Error uploading file.");
        }
        res.end("File is uploaded successfully!");
    });
});

// este endpoint se encarga de recibir el formulario de subida de archivos xlsx
// con el objetivo de guardarlos en el directorio ./fileCoordenadas
app.post('/xlsxCoordenadas',function(req,res){
    upload2(req,res,function(err) {
        if(err) {
            return res.end("Error uploading file.");
        }
        res.end("Archivo subido con éxito");
    });
});

/**
* Promises ::: se crean las siguientes promesas para tratar de
* cumplir una secuencia de pasos individiales (promesa) hasta
* completar un objetivo comun (subir archivo y responder json).
* :: :: route:: POST /apikmz/xlsx-tendido-fase1
*/
function recibirArchivo(req, res) {
    var prom = new Promise ( function (resolve, reject) {
        var storage3 =   multer.diskStorage({
          destination: function (req, file, callback) {
            callback(null, './img4kmz/temp');
          },
          filename: function (req, file, callback) {
            callback(null, file.originalname);
          }
        });
        var upload3 = multer({ storage : storage3}).single('myfile');
        upload3(req,res,function(err) {
            if(err) {
                //return res.end("Error uploading file.");
                return reject(err);
            }
            //res.end("Archivo subido con éxito");
            return resolve(req.file.originalname);
        });
    });
    return prom;
}

var managerXlsx = require('excel');

function xlsxToTendido (filename) {
  var prom = new Promise ( function (resolve, reject) {
      managerXlsx('./img4kmz/temp/'+filename, '1',
          function (err, data)
          {
            if (err) reject({tipo:'xlsxToTendido', error: err});
            var _tipo, _id, _latitude, _longitude, _secuencia = "";
            var tendido = [];
            data.forEach ( function (row, i, data) {
              if (i>0) {
                _tipo = row[0]; _tipo = _tipo.trim();
                _id = row[1]; _id = _id.trim();
                _latitude = row[2]; _latitude = _latitude.trim();
                _longitude = row[3]; _longitude = _longitude.trim();
                _secuencia = row[4]; _secuencia = _secuencia.trim();
                if (_tipo && _tipo.toUpperCase == "MUFA") {
                  _tipo = "Mufa";
                } else if (_tipo && _tipo.toUpperCase == "POSTE") {
                  _tipo = "Poste";
                } else if (_tipo && _tipo.toUpperCase == "CLIENTE") {
                  _tipo = "Cliente";
                }

                if (_tipo) {
                  tendido.push({
                    tipo1:_tipo,
                    id:_id,
                    longitudeSA:"",
                    distanciaA:"",
                    latitude:_latitude,
                    longitude:_longitude,
                    secuencia1:_secuencia,
                    secuencia2:"",
                    totalreserva:"",
                    cruceta:"",
                    postesnuevos:"",
                    mufanueva:"",
                    instalacionDB:"",
                    preformadoDR:"",
                    suspencion:"",
                    notas:"",
                    images: {
                      img1 : "",
                      img2 : "",
                      img3 : "",
                      img4 : "",
                      img5 : "",
                      img6 : "",
                      img7 : ""
                    }
                  });
                }
              }
            }); //fin forEach

            tendido.push({
              tipo1:"Cable",
              id:"Cable",
              longitudeSA:"",
              distanciaA:"",
              latitude:"",
              longitude:"",
              secuencia1:"",
              secuencia2:"",
              totalreserva:"",
              cruceta:"",
              postesnuevos:"",
              mufanueva:"",
              instalacionDB:"",
              preformadoDR:"",
              suspencion:"",
              notas:"",
              images: {
                img1 : "",
                img2 : "",
                img3 : ""
              }
            });
            resolve(tendido);
          }
      );
  });
  return prom;
}

function borrarArchivo(filename) {
  var prom = new Promise( function (resolve, reject) {
    var _fs = require("fs");
    _fs.unlink ( filename, function (err) {
       if (err) {
           reject({tipo:'borrarArchivo', error: err});
       }
       resolve();
    });
  });

  return prom;
}


/**
* Route /apikmz/xlsx-tendido-fase1
* Recibe un excel de entrada, con un formato establecido, con el fin de
* leer automaticamente el listado de Mufa, poste n, n+1, n+2, etc.
* hasta llegar al ultimo poste, Cliente.
*/
app.post("/apikmz/xlsx-tendido-fase1", function (req, res) {
  //console.log(req);
  // upload3(req,res,function(err) {
  //     if(err) {
  //         return res.end("Error uploading file.");
  //     }
  //     res.end("Archivo subido con éxito");
  // });
    var _filename, _data;

    recibirArchivo(req, res)
    .then( function (filename) {
      _filename = filename;
      return xlsxToTendido (filename);
    })
    .then( function (data) {
      _data = data;
      return borrarArchivo('./img4kmz/temp/'+_filename);
    })
    .then( function () {
      res.json({success:true, message:"Archivo recibido:: "+_filename, data:_data});
    })
    .catch( function (error) {
      res.json({success:false, error: error});
    });

});

var fs = require("fs"),
    path = require("path");

app.get('/getFileList', function (req, res, next) {
  var p = "./uploads";
  var filelist = [];
  fs.readdir(p, function (err, files) {
      if (err) {
          throw err;
      }

      files.map(function (file) {
          return path.join(p, file);
      }).filter(function (file) {
          return fs.statSync(file).isFile();
      }).forEach(function (file) {
          filelist.push({ archivo:""+path.basename(file), ruta:file });
          console.log("%s (%s)", file, path.basename(file));

      });
      res.json({fileList:filelist});
  });
});

// para manejar archivos excel
var parseXlsx = require('excel');

app.post('/leer-xlsx', function (req, res, next) {
  if (req.body.filename) {
      console.log("filename: ", req.body.filename);
      parseXlsx('./uploads/'+req.body.filename, '1', function(err, data) {
        if(err) res.json({success:false, error:err});
          // data is an array of arrays
          var c = 0;
          data.forEach(function(elem) {
            c++;
            // con saltar 1 fila, omito los titulos de la tabla.
            if (c>1) {
                /*
                // para crear mientras va leyendo fila x fila
                var newMunicipio = new Municipio({
                    codigo: elem[0],
                    nombre: elem[1],
                    depto: elem[2],
                    tiempo: elem[3],
                    rango: elem[4]
                });
                newMunicipio.save();
                */

                //** actualiza tiempo y rango, mientras va leyendo
                Municipio.findOne({codigo:elem[0], nombre:elem[1], depto:elem[2]}, function (err, municipio) {
                    if (!err) {
                        if(municipio) {
                        municipio.tiempo = elem[3];
                        municipio.rango = elem[4];
                        municipio.save();
                      } else {
                        console.log("no municipio -",elem[0], elem[1], elem[2], elem[3], elem[4]);
                      }
                    }
                });

            }
          });
          //res.json({success:true, contenido:data});
          res.json({success:true});
      });
  } else {
    res.json({success:false});
  }


});

app.get('/buscar-muni/:muni', function (req, res, next) {
  if( req.params.muni) {
    var nombreMuni = req.params.muni.toLowerCase().trim();
    nombreMuni = getCleanedString(nombreMuni);
    var d = [];
    Municipio.find({}, function (err, municipios) {
      if (err) {
        res.json({success:false, error:err});
      } else {
        municipios.forEach( function (elem) {
          var dbmuni = getCleanedString(elem.nombre.toLowerCase().trim() );
          if (dbmuni==nombreMuni ){
            d.push(elem);
          }
        });
        res.json({success:true, municipios:d});
      }
    });
  } else {
    res.json({success:false, error: 'incluya el parametro muni'});
  }
});
//pendinetes
//message.trim() eliminar espacios en blanco y /t /r etc

//esta otra linea apoya a la comparacion sin importar mayusculas
//db.getCollection('municipios').find({nombre: {$regex: 'huite', $options: "i"}})

//leer el kml original y extraer las coordenadas del punto y del poligono para insertarlas en la base de datos
//resta nada mas que redactar de nuevo el gran xml para devolver el kml file listo.



var file_inicio = '<?xml version="1.0" encoding="UTF-8"?>'
+'<!-- Generated by Feature Manipulation Engine 2012 SP1 (Build 12224) -->'
+'<kml xmlns="http://www.opengis.net/kml/2.2" xmlns:atom="http://www.w3.org/2005/Atom" xmlns:gx="http://www.google.com/kml/ext/2.2">'
+'<Document>'
+'<name>Municipios</name>'
+'<visibility>1</visibility>'
+'<Folder id="kml_ft_Limites_Municipales">'
+'<name>Limites_Municipales</name>"';
var file_fin = '</Folder></Document></kml>';
/*
placemark_par['@'].id =
placemark_par['name'] =
placemark_par.Style.PolyStyle.color =
placemark_par.Polygon.outerBoundaryIs.LinearRing.coordinates =
*/
var placemark_par =
        {
          "@" : {"id":"kml_2"},
          "name":"HUITE",
          "visibility" : "1",
          "Style" : {
            "IconStyle" : {
              "scale" : "0.8",
              "Icon" : {
                "href" : "http://maps.google.com/mapfiles/kml/shapes/placemark_circle.png"
              }
            },
            "LabelStyle" : {
              "scale" : "1.0"
            },
            "LineStyle" : {
              "color" : "",
              "width" : "2"
            },
            "PolyStyle" : {
              "color" : "3300aaff"
            }
          },
          "Polygon" : {
            "extrude" : "0",
            "tessellate" : "1",
            "outerBoundaryIs" : {
              "LinearRing" : {
                "coordinates" : "-90.8642842311222,14.8083714884016,0 -90.8695809087851,14.8118451500473,0 -90.8774925410162,14.8106531775224,0 -90.8803886719318,14.8096960684928,0 -90.8859655378648,14.8056822526161,0 -90.8899936164185,14.8033359413619,0 "
              }
            }
          }
        };
/*
placemark_impar['@'].id =
placemark_impar['name'] =
placemark_impar.Style.PolyStyle.color =
placemark_impar.Point.coordinates =
*/
var placemark_impar =
				{
					"@":{"id":"kml_1"},
					"name":"HUITE",
					"Style" : {
						"IconStyle" : {
							"scale" : "0.8",
							"Icon" : {
								"href" : "http://maps.google.com/mapfiles/kml/shapes/placemark_circle.png"
							}
						},
						"LabelStyle" : {
							"scale" : "1.0"
						},
						"LineStyle" : {
							"color" : "",
							"width" : "2"
						},
						"PolyStyle" : {
							"color" : "3300aaff"
						}
					},
					"Point" : {
						"coordinates" : "-89.7000732421875,14.9097852706909,0"
					}
				};
var js2xmlparser = require("js2xmlparser");

function getImpar (obj) {
  var mun = js2xmlparser.parse("Placemark", obj);
  mun = mun.replace("<?xml version='1.0'?>","");
  return mun;
}
function getPar (obj) {
  var mun = js2xmlparser.parse("Placemark", obj);
  mun = mun.replace("<?xml version='1.0'?>","");
  return mun;
}
function colorToCode (color) {
  color = color.toUpperCase();
  switch (color) {
    case "AZUL":
      return "7fff0000";
      break;
    case "VERDE":
      return "7f00ff55";
      break;
    case "AMARILLO":
      return "7f00ffff";
      break;
    case "ANARANJADO":
      return "7f0055ff";
      break;
    case "ROJO":
      return "7f0000aa";
      break;
    default:

  }
}


app.get('/generarKML', function (req, res, next) {
  Municipio.find({}, function (err, municipios) {
    if (err) {
      res.json({success:false});
    } else {
      var es = 0;
      var par, impar = {};
      var logStream = fs.createWriteStream('./public/resultado.kml', {'flags': 'w'});
      logStream.write(file_inicio);
      municipios.forEach( function(_muni) {
        es++;
        placemark_impar['@'].id = _muni.numImpar;
        placemark_impar['name'] = _muni.nombre;
        placemark_impar.Style.PolyStyle.color = colorToCode(_muni.rango);
        placemark_impar.Point.coordinates = _muni.point;
        //getImpar(placemark_impar);
        logStream.write(getImpar(placemark_impar));

        placemark_par['@'].id = _muni.numPar;
        placemark_par['name'] = _muni.nombre;
        placemark_par.Style.PolyStyle.color = colorToCode(_muni.rango);
        placemark_par.Polygon.outerBoundaryIs.LinearRing.coordinates = _muni.linearRing;
        //getPar(placemark_par);
        logStream.write(getPar(placemark_par));
      });

      logStream.end(file_fin);

      //var filePath = __dirname+ '/public/resultado.kml';
      /*var stat = fs.statSync(filePath);

      res.writeHead(200, {
          'Content-Type': 'text/xml',
          'Content-Length': stat.size
      });

      var readStream = fs.createReadStream(filePath);*/
      // We replaced all the event handlers with a simple call to readStream.pipe()
      //readStream.pipe(res);
      //res.download(filePath);
      res.json({success:true});
    }
  });


});


/*
** Para generar el KML de los puntos
**
** Objetos necesarios:
*/
var file_inicio_points = '<?xml version="1.0" encoding="UTF-8"?>'
+'<kml xmlns=\'http://earth.google.com/kml/2.1\'>'
+'<Folder id=\'layer fmain\'>'
+'<name>KML</name>'
+'<visibility>0</visibility>'
+'<open>0</open>';
var file_fin_points = '</Folder></kml>';
var Placemark_point = {
  "@" : {
    "id" : "layer p"
  },
  "StyleMap" : {
    "@" : {
      "id":'myicon_4'
    },
    "Pair" : [
      {
        "key":"normal",
        "styleUrl":"#sn_"
      },
      {
        "key":"highlight",
        "styleUrl":"#sn_"
      }
    ]
  },
  "Style" : [
    {
      "@" : {
        "id":"sn_"
      },
      "IconStyle" : {
        "scale":"1.1",
        "Icon":"http://maps.google.com/mapfiles/kml/pushpin/red-pushpin.png"
      },
      "ListStyle":{
        "width" : "5"
      }
    },
    {
      "@" : {
        "id":"sh_"
      },
      "IconStyle" : {
        "scale":"1.3",
        "Icon":"http://maps.google.com/mapfiles/kml/pushpin/red-pushpin.png"
      },
      "ListStyle":{}
    }
  ],
  "name":"N39107",
  "visibility":"0",
  "Snippet":{
    "@": {
      "maxLines":"0",
      "id":"s"
    }
  },
  "LookAt":{
    "longitude" : "",
    "latitude" : "",
    "altitude" : "0",
    "range" : "1000",
    "tilt" : "0",
    "heading" : "0",
    "altitudeMode" : "relativeToGround"
  },
  "styleUrl":"#myicon_",
  "Point":{
    "coordinates" : ""
  }
};

var placemark_hilos = {
  "name" : "",
  "description" : "",
  //"styleUrl" : "",
  "Style" : {
    "LineStyle" : {
      "width" : "5"
    }
  },
  "LineString" : {
    "width" : "5",
    "tessellate" : "1",
    "coordinates" : ""
  }
};

//  -> Warning ::: este endpoint quedo obsoleto, se sustituyo por /leer-xlsx-coordenadas
// esto se usa en la pestaña de coordenadas
// recibe en texto, las coordenadas y escribe un archivo kml
// que contiene las coordenadas puestas en el mapa.
app.post("/kml-points", function (req, res, next) {
  var datestring = new Date().toISOString();
  datestring = datestring.replace(":","-");
  datestring = datestring.replace(":","-");
  var logStream = fs.createWriteStream('./fileCoordenadas/result/'+datestring+'.kml', {'flags': 'w'});
  logStream.write(file_inicio_points);
  if ( req.body.points) {
    var place = CLONE(Placemark_point);
    var points = req.body.points;
    var c1 = 1;
    for(var p in points) {
      console.log(p);
        place["@"].id = "layer p"+c1;
        place.StyleMap["@"].id = "myicon_" + c1;
        place.StyleMap.Pair[0].styleUrl = "sn_"+c1+"_1";
        place.StyleMap.Pair[1].styleUrl = "sn_"+c1+"_2";
        place.Style[0]["@"].id = "sn_" + c1;
        place.Style[1]["@"].id = "sh_" + c1;
        place.Snippet["@"].id = "s" + c1;
        place.LookAt.longitude = points[p].uno;
        place.LookAt.latitude  = points[p].dos;
        place.styleUrl = "myicon_" + c1;
        place.Point.coordinates = points[p].uno+","+points[p].dos+",0";
        var foo = js2xmlparser.parse("Placemark", place);
        foo = foo.replace("<?xml version='1.0'?>","");
        logStream.write(foo);
        c1++;
    }



    logStream.end(file_fin_points);
    res.json({success:true, filename:datestring + ".kml"});
  } else {
    res.json({success:false});
  }
});


// para leer el archivo xlsx subido para leer sus coordenadas
app.post('/leer-xlsx-coordenadas', function (req, res, next) {
  // con el cambio a POST
  //  /:filename/:iconurl
  // ahora filename e iconurl vienen por req.body

    // variables para extraer datos fila x fila del excel xlsx
    var nombre, latitude, longitude, descripcion, place;
    // variables para convertir
    var _longitude, _latitude, arrlong, arrlat, cor;
    cor=1;
    // recolectora de coordenadas para hacer la ruta
    var coordenadas_ruta = "";
    // variables para el manejo del archivo .kml a redactarse
    var datestring = new Date().toISOString();
    datestring = datestring.replace(":","-");
    datestring = datestring.replace(":","-");
    var logStream = fs.createWriteStream('./fileCoordenadas/result/'+datestring+'.kml', {'flags': 'w'});
    logStream.write(file_inicio_points);

    if (req.body.filename && req.body.iconurl) {
      // si el nombre viene, se abre y sus filas vienen en un arreglo denominado "data"
      parseXlsx('./fileCoordenadas/'+req.body.filename, '1',
      function (err, data)
      {
          if (err) res.json({success:false, error: err});

          var igrados, iminutos, isegs;
          // iteramos el contenido del arreglo "data", a partir de las posiciones mayores a 1 (encabezado)
          var c = 0;
          data.forEach ( function (elem) {
            c++;
            if (c>1) {

              // extracción de los datos de la fila del xlsx
              nombre = elem[0] + " - " + elem[1];
              latitude = elem[2];
              longitude = elem[3];
              descripcion = elem[4] || elem[0] + " " + elem[1];

              place = CLONE(Placemark_point);
              place["@"].id = "layer p"+(c-1);
              place.StyleMap["@"].id = "myicon_" + (c-1);
              place.StyleMap.Pair[0].styleUrl = "sn_"+(c-1)+"_1";
              place.StyleMap.Pair[1].styleUrl = "sn_"+(c-1)+"_2";
              place.Style[0]["@"].id = "sn_" + (c-1);
              place.Style[1]["@"].id = "sh_" + (c-1);
              place.Snippet["@"].id = "s" + (c-1);
              place["Style"][0]["IconStyle"].Icon = req.body.iconurl;
              place["Style"][1]["IconStyle"].Icon = req.body.iconurl;
              place.visibility = 1;
              place.name = nombre;
              place.description = descripcion;

              if (longitude.charAt(0)==="-"){ cor = -1; }

              igrados  = longitude.indexOf("°");
              iminutos = longitude.indexOf("'");
              isegs    = longitude.indexOf("\"");
              if (igrados>-1 || iminutos>-1 || isegs>-1 ){
                // está en grados minutos segundos

                if (longitude.charAt(igrados+1)===" " )
                  { //longitude = longitude.replace("°", "");
                  }
                else { longitude = longitude.replace("°", "° "); }
                if (longitude.charAt(iminutos+1)===" " )
                  { //longitude = longitude.replace("'", "");
                  }
                else { longitude = longitude.replace("'", "' "); }
                if (longitude.charAt(isegs+1)===" " )
                  { //longitude = longitude.replace("\"", "");
                  }
                else { longitude = longitude.replace("\"", "\" "); }

                if (longitude.endsWith("N") || longitude.endsWith("S") || longitude.endsWith("E") || longitude.endsWith("W") )
                {
                  longitude = longitude.replace("N","");
                  longitude = longitude.replace("S","");
                  longitude = longitude.replace("E","");
                  //longitude = longitude.replace("W","");
                  if (longitude.charAt(0)!=="-" && longitude.endsWith("W") ){
                    console.log(longitude);
                    cor = -1;
                    longitude = "-"+longitude; }
                }

                longitude = longitude.trim();
                arrlong = longitude.split(" ");
                _longitude = parseFloat(arrlong[0]) + (parseFloat(arrlong[1]) / (cor*60)) + (parseFloat(arrlong[2]) / (cor*3600));
                longitude = _longitude;
              }

              cor = 1;
              igrados  = latitude.indexOf("°");
              iminutos = latitude.indexOf("'");
              isegs    = latitude.indexOf("\"");
              if (latitude.indexOf("°")>-1 || latitude.indexOf("'")>-1 || latitude.indexOf("\"")>-1 ){
                // está en grados minutos segundos

                if (latitude.charAt(igrados+1)===" " )
                  { //latitude = latitude.replace("°", "");
                  }
                else { latitude = latitude.replace("°", "° "); }
                if (latitude.charAt(iminutos+1)===" " )
                  { //latitude = latitude.replace("'", "");
                  }
                else { latitude = latitude.replace("'", "' "); }
                if (latitude.charAt(isegs+1)===" " )
                  { //latitude = latitude.replace("\"", "");
                  }
                else { latitude = latitude.replace("\"", "\" "); }

                if (latitude.endsWith("N") || latitude.endsWith("S") || latitude.endsWith("E") || latitude.endsWith("W") )
                {
                  latitude = latitude.replace("N","");
                  latitude = latitude.replace("S","");
                  latitude = latitude.replace("E","");
                  latitude = latitude.replace("W","");

                }

                latitude = latitude.trim();
                arrlat = latitude.split(" ");
                _latitude = parseFloat(arrlat[0]) + (parseFloat(arrlat[1]) / (60)) + (parseFloat(arrlat[2]) / (3600));
                latitude =  _latitude;
              }
              if (req.body.ruta) {
                coordenadas_ruta += "" + longitude+","+latitude+",0 ";
              }

              place.LookAt.longitude = longitude;
              place.LookAt.latitude  = latitude;
              place.styleUrl = "myicon_" + (c-1);
              place.Point.coordinates = longitude+","+latitude+",0";
              var foo = js2xmlparser.parse("Placemark", place);
              foo = foo.replace("<?xml version='1.0'?>","");
              logStream.write(foo);
            }

          }); // termina el forEach

          if (req.body.ruta && req.body.ruta == 'SI') {
            place = CLONE(placemark_hilos);
            place.name = "Ruta";
            place["LineString"]["coordinates"] = coordenadas_ruta;
            var foo2 = js2xmlparser.parse("Placemark", place);
            foo2 = foo2.replace("<?xml version='1.0'?>","");
            logStream.write(foo2);
          }

          logStream.end(file_fin_points);
          res.json({success:true, filename:datestring + ".kml"});
      });
    } else {
      res.json({success:false});
    }

}); // fin GET /leer-xlsx-coordenadas/:filename

/* fin del componente a exportar*/
}
