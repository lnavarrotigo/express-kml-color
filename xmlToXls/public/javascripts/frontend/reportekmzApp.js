angular.module("reportekmzApp", ["720kb.datepicker"])

.controller("reportekmzController", function ($scope, $http) {

  $scope.items = ['paso1', 'paso2', 'paso3', 'paso4'];
  $scope.selection = $scope.items[0];

  $scope.setPaso = function (paso) {
    $scope.selection = paso;
    //console.log($rootScope);
  };

  $scope.$on('to_parent', function (event, data) {
    $scope.setPaso(data);
  });

  $scope.$on('to_parent_paso1', function(event, data) {
    $scope.$broadcast('to_paso1', data);
  });

})

.controller("buscarCtrl", function ($scope, $http, DataFc) {
  var ctrl = this;
  ctrl.f ="";

  ctrl.b = function () {
    if (ctrl.f) {
      $http.get("/apikmz/reporte/"+ctrl.f)
      .then(function (res) {
        if (res.data.success) {
          var r = res.data.reporte;
          DataFc.idreporte = res.data.reporte._id;
          //delete r._id;
          //delete r.__v;
          console.log("Respondio success /apikmz/reporte/:id",r);
          DataFc.data.empresa = r.empresa;
          DataFc.data.instalacion = r.instalacion;
          DataFc.data.ot = r.ot;
          DataFc.data.nombreCliente = r.nombreCliente;
          DataFc.data.direccion = r.direccion;
          DataFc.data.codigoLargoMufaNodo = r.codigoLargoMufaNodo;
          DataFc.data.idmufa = r.idmufa;
          DataFc.data.codigoPuertoSplitter = r.codigoPuertoSplitter;
          DataFc.data.secuenciaInicial = r.secuenciaInicial;
          DataFc.data.secuenciaFinal = r.secuenciaFinal;
          DataFc.data.hiloProcedencia = r.hiloProcedencia;
          DataFc.data.hiloDestino = r.hiloDestino;

          DataFc.data.habilitacion =     r.habilitacion;
          DataFc.data.enrutado =         r.enrutado;
          DataFc.data.tendido =          r.tendido;
          DataFc.data.conectorizacion =  r.conectorizacion;
          DataFc.data.factibilidadDeServicio = r.factibilidadDeServicio;
          DataFc.data.postes =           r.postes;
          DataFc.data.retiroFO =         r.retiroFO;
          DataFc.data.ducto =            r.ducto;
          DataFc.data.instalacionCT =    r.instalacionCT;
          DataFc.data.traslado =         r.traslado;

          DataFc.data.listaPostes = r.listaPostes;
          DataFc.data.kml = r.kml;
          DataFc.data.folder = r.folder;

          if (r.fechaInstalacion) {
            console.log(typeof r.fechaInstalacion);
            var finst = r.fechaInstalacion.split("T");
            finst = finst[0];
            finst =  finst.replace("-","/");
            finst =  finst.replace("-","/");
            finst = finst.split("/");
            finst = ""+finst[2]+"/"+finst[1]+"/"+finst[0];
            DataFc.data.fechaInstalacion = finst;
          }

          if (r.fechainicio) {
          var fechainicio_ = r.fechainicio.split("T");
            fechainicio_ = fechainicio_[0];
            fechainicio_ = fechainicio_.replace("-","/");
            fechainicio_ = fechainicio_.replace("-","/");
            fechainicio_ = fechainicio_.split("/");
            fechainicio_ = ""+fechainicio_[2]+"/"+fechainicio_[1]+"/"+fechainicio_[0];
            DataFc.data.fechainicio = fechainicio_;
          }

          if (r.fechafin) {
            var fechafin_ = r.fechafin.split("T");
            fechafin_ = fechafin_[0];
            fechafin_ = fechafin_.replace("-","/");
            fechafin_ = fechafin_.replace("-","/");
            fechafin_ = fechafin_.split("/");
            fechafin_ = ""+fechafin_[2]+"/"+fechafin_[1]+"/"+fechafin_[0];
            DataFc.data.fechafin = fechafin_;
          }
          //DataFc.data.fechainicio = r.fechainicio;
          //DataFc.data.fechafin  = r.fechafin;
          //delete DataFc.data._id;
          //delete DataFc.data.__v;
          console.log(DataFc);
          $scope.$emit('to_parent', 'paso1');
          $scope.$emit('to_parent_paso1', DataFc.idreporte);
        }
      });
    }
  };
})

.controller("paso1Ctrl", function($scope, $http, DataFc) {
  var ctrl = this;
  $scope.p_so1=1;
  //DataFc =
  ctrl.v = DataFc.data;

  $scope.$on('to_paso1', function (event, data) {
    ctrl.idreporte = data;
  });

  ctrl.submit =  function () {
    DataFc.data = ctrl.v;
    console.log(DataFc.data);
    if (!ctrl.idreporte) {
      if (!ctrl.v.hiloProcedencia) {
        ctrl.v.hiloProcedencia = '-No Disponible-';
      }
      if (!ctrl.v.hiloDestino) {
        ctrl.v.hiloDestino = '-No Disponible-';
      }
      DataFc.data = ctrl.v;
      console.log(DataFc.data);
      // peticion http POST
      $http.post("/apikmz/reporte", ctrl.v)
      .then ( function (res) {
        if (res.data.success) {
          DataFc.idreporte = res.data.reporte._id;

          //crear el directorio para subir los archivos
          $http.get("/kmGenerateDoc/"+DataFc.idreporte+"/"+ctrl.v.nombreCliente)
          .then( function (res) {
            if (res.data.success) {
              DataFc.data.kml    = res.data.src;
              DataFc.data.folder = res.data.folder;
              console.log(res.data);
            }
          });

          ctrl.idreporte = DataFc.idreporte;
          $scope.$emit('to_parent', 'paso2');
          console.log("reporte generado:",ctrl.idreporte);
        }
      }); // termina peticion http POST
    } else {
      // peticion PUT
      $http.put("/apikmz/reporte/"+ctrl.idreporte, ctrl.v)
      .then(function (res) {
        $scope.$emit('to_parent', 'paso2');
        console.log(res.data);
      }); // termina peticion http PUT
    }

  };

  ctrl.siguiente = function () {
    $scope.$emit('to_parent', 'paso2');
  };

  ctrl.idreporte = DataFc.idreporte;
  if(ctrl.idreporte) {
    console.log(ctrl.idreporte);
  }

}) // --fin controlador paso1Ctrl

.controller("paso2Ctrl", function ($scope, $http, DataFc) {
  var ctrl = this;
  $scope.p_so2=2;
  ctrl.v = DataFc.data;
  ctrl.idreporte = DataFc.idreporte;
  ctrl.reporteActualizado = false;

  ctrl.submit = function () {
    DataFc.data = ctrl.v;
    if (ctrl.idreporte) {
      $http.put("/apikmz/reporte/"+ctrl.idreporte, ctrl.v)
      .then(function (res) {
        ctrl.reporteActualizado = res.data.success;
        $scope.$emit('to_parent', 'paso3');
      });
    }
  };

}) // --fin controlador paso2Ctrl

.controller("paso3Ctrl", function ($scope, $http, DataFc, FileS) {
  var ctrl = this;
  $scope.p_so3=3;
  ctrl.datareceived = "";
  ctrl.v = DataFc.data;
  ctrl.idreporte = DataFc.idreporte;
  ctrl.l = [];
  ctrl.li = {};
  ctrl.images = {
    img1 : "",
    img2 : "",
    img3 : "",
    // se agregan mas elementos de fotos, para el tipo mufa, cable y cliente
    img4 : "",
    img5 : "",
    img6 : "",
    img7 : "",
  };

  ctrl.guardarycontinuar = false;

  if (ctrl.v && ctrl.v.listaPostes) {
    ctrl.l = ctrl.v.listaPostes;
  }

  ctrl.initLi = function () {
      ctrl.li = {
        tipo1:"",
        id:"",
        longitudeSA:"",
        distanciaA:"",
        latitude:"",
        longitude:"",
        secuencia1:"",
        secuencia2:"",
        totalreserva:"",
        cruceta:"",
        postesnuevos:"",
        mufanueva:"",
        instalacionDB:"",
        preformadoDR:"",
        suspencion:"",
        notas:"",
        images: {
          img1 : "",
          img2 : "",
          img3 : "",
          // se agregan mas elementos de fotos, para el tipo mufa, cable y cliente
          img4 : "",
          img5 : "",
          img6 : "",
          img7 : "",
        }
      };
  }; ctrl.initLi();

  ctrl.addli = function () {
    console.log("ingresa a addli", ctrl.li);
    //if (ctrl.li.tipo1 && ctrl.li.id && ctrl.li.latitude && ctrl.li.longitude) {
    if (ctrl.li.tipo1 && ctrl.li.id && ctrl.li.latitude && ctrl.li.longitude) {
      ctrl.l.push(ctrl.li);
      ctrl.initLi();
    } else {
      document.getElementById('tipo1').focus();
    }
  };

  ctrl.submit = function () {
    ctrl.v.listaPostes = ctrl.l;
    DataFc.data = ctrl.v;
    console.log(DataFc.data);
    if (ctrl.idreporte) {
      $http.put("/apikmz/reporte/"+ctrl.idreporte, ctrl.v)
      .then(function (res) {
        ctrl.reporteActualizado = res.data.success;
        ctrl.guardarycontinuar = true;
        //$scope.$emit('to_parent', 'paso4');
      });
    }
  };

  $scope.del = function (item) {
    var foo = false;
    var i = ctrl.l.indexOf( item );
    //ctrl.l.splice( i, 1 );
    //console.log("vamos a borrar los archivos:",item);
    $("input").prop('disabled', true);
    if (item.images && item.images.img1 && item.images.img1!=="") {
      console.log("borrar ", item.images.img1);
      FileS.delete(item.images.img1)
      .then( function (res) {
        if (res.data.success) {
          item.images.img1="";
          if (item.images.img2==="" && item.images.img3==="")
          { ctrl.l.splice( i, 1 ); $("input").prop('disabled', false); }
        } else { ctrl.l.splice( i, 1 ); $("input").prop('disabled', false); }
      });
    } else { ctrl.l.splice( i, 1 ); $("input").prop('disabled', false); }
    if (item.images && item.images.img2 && item.images.img2!=="") {
      console.log("borrar ", item.images.img2);
      FileS.delete(item.images.img2)
      .then( function (res) {
        if (res.data.success) {
          item.images.img2="";
          if (item.images.img1==="" && item.images.img3==="")
          { ctrl.l.splice( i, 1 ); $("input").prop('disabled', false); }
        } else { ctrl.l.splice( i, 1 ); $("input").prop('disabled', false); }
      });
    } else { ctrl.l.splice( i, 1 ); $("input").prop('disabled', false); }
    if (item.images && item.images.img3 && item.images.img3!=="") {
      console.log("borrar ", item.images.img3);
      FileS.delete(item.images.img3)
      .then( function (res) {
        if (res.data.success) {
          item.images.img3="";
          if (item.images.img1==="" && item.images.img2==="")
          { ctrl.l.splice( i, 1 ); $("input").prop('disabled', false); }
        } else { ctrl.l.splice( i, 1 ); $("input").prop('disabled', false); }
      });
    } else { ctrl.l.splice( i, 1 ); $("input").prop('disabled', false); }

  };

  $scope.setTipo = function (tipo) {
    ctrl.li.tipo1 = tipo;
    if (tipo==="Cable") {
      ctrl.li.id = "--cable--";
      ctrl.li.latitude = "--cable--";
      ctrl.li.longitude = "--cable--";
      ctrl.li.secuencia1 = "";
    } else if (tipo==="Mufa") {
      ctrl.li.secuencia1 = ctrl.v.secuenciaInicial;
      ctrl.li.id = ctrl.v.idmufa;
      ctrl.li.latitude = "";
      ctrl.li.longitude = "";
    } else if (tipo==="Cliente") {
      ctrl.li.secuencia1 = ctrl.v.secuenciaFinal;
      ctrl.li.id = ctrl.v.instalacion;
      ctrl.li.latitude = "";
      ctrl.li.longitude = "";
    } else {
      ctrl.li.id = "";
      ctrl.li.latitude = "";
      ctrl.li.longitude = "";
      ctrl.li.secuencia1 = "";
    }
  };

  ctrl.tendido = function () {
    var _tend = $("#datareceived").val();
    var tend = JSON.parse(_tend);
    //console.log("el tendido es::: ",tend.data);
    ctrl.l = [];
    tend.data.forEach( function (elem) {
      ctrl.l.push(elem)
    });
    $("#submitDatareceived").hide(500);
  };
  // ctrl.addli = function () {
  //   console.log("ingresa a addli", ctrl.li);
  //   //if (ctrl.li.tipo1 && ctrl.li.id && ctrl.li.latitude && ctrl.li.longitude) {
  //   if (ctrl.li.tipo1 && ctrl.li.id && ctrl.li.latitude && ctrl.li.longitude) {
  //     ctrl.l.push(ctrl.li);
  //     ctrl.initLi();
  //   } else {
  //     document.getElementById('tipo1').focus();
  //   }
  // };

}) // --fin controlador paso3Ctrl

.controller("paso4Ctrl", function ($scope, $http, DataFc, FileS) {
  console.log("empezamos el controlador del paso4");
  var ctrl = this;
  ctrl.v = DataFc.data;
  ctrl.idreporte = DataFc.idreporte;
  ctrl.la = [];
  ctrl.lai = {};

  if (ctrl.v && ctrl.v.listaPostes) {
    ctrl.l = ctrl.v.listaPostes;
    if (!ctrl.v.kml && !ctrl.v.folder && ctrl.idreporte) {
      //crear el directorio para subir los archivos
      $http.get("/kmGenerateDoc/"+ctrl.idreporte+"/"+ctrl.v.nombreCliente)
      .then( function (res) {
        if (res.data.success) {
          DataFc.data.kml    = res.data.src;
          DataFc.data.folder = res.data.folder;
          ctrl.v.kml = DataFc.data.kml;
          ctrl.v.folder = DataFc.data.folder;
          console.log(res.data);
        }
      });
    }
  }
  //ctrl.l = ctrl.v.listaPostes;

  ctrl.initLai = function () {
      ctrl.lai = {
        cantidad:"",
        item:"",
        descripcionItem:"",
        conIva:"",
        totalSinIva:"",
        totalConIva:""
      };
  };
  ctrl.addlai = function () {
    ctrl.la.push(ctrl.lai);
    ctrl.initLai();
  };

  ctrl.submit = function () {
    // ctrl.v.listaActividades = ctrl.la;
    // DataFc.data = ctrl.v;
    // console.log(DataFc.data);
    if (ctrl.idreporte) {
      console.log(ctrl.v);
      $http.put("/apikmz/reporte/"+ctrl.idreporte, ctrl.v)
      .then(function (res) {
        ctrl.reporteActualizado = res.data.success;
        $scope.$emit('to_parent', 'paso5');
        // if (ctrl.reporteActualizado) {
        //   $http.put("/apikmz/download/"+ctrl.idreporte, ctrl.v)
        //   .then(function (res) {
        //     if (res.data.success) {
        //       console.log("puede descargar el kmz");
        //       console.log("/kmz.kmz");
        //     }
        //   });
        // }
      });
    }
  };


  // seccion para captar los archivos a subir
  $scope.file_changed1 = function(element) {
    console.log(element.files);

       $scope.$apply(function(scope) {
           var photofile = element.files[0];
           var reader = new FileReader();
           reader.onload = function(e) {
              // handle onload
           };
           var urlFile = reader.readAsDataURL(photofile);
           console.log(urlFile);
       });
  };

  /**
  * Configura en ctrl.v.listaPostes, en el arreglo images,
  * busca en que posicion de la listaPostes se va a llenar el path
  * para la imagen correspondiente.
  */
  $scope.changeUp = function (elemid, tipo1, id, ord) {
    console.log(elemid+" -- "+tipo1+" -- "+id+" -- "+ord);
    //$('#input-file'+num).on('change', function(){
    var findIndex = function (dirArchivo, ord_){
        for (var i=0; i < ctrl.v.listaPostes.length; i++) {

            if (ctrl.v.listaPostes[i].tipo1 === tipo1
            && ctrl.v.listaPostes[i].id === id) {

              if (dirArchivo) {
                switch (ord_) {
                  case 1:
                    ctrl.v.listaPostes[i].images.img1=dirArchivo;
                    break;

                  case 2:
                    ctrl.v.listaPostes[i].images.img2=dirArchivo;
                    break;

                  case 3:
                    ctrl.v.listaPostes[i].images.img3=dirArchivo;
                    break;

                  case 4:
                    ctrl.v.listaPostes[i].images.img4=dirArchivo;
                    break;

                  case 5:
                    ctrl.v.listaPostes[i].images.img5=dirArchivo;
                    break;

                  case 6:
                    ctrl.v.listaPostes[i].images.img6=dirArchivo;
                    break;

                  case 7:
                    ctrl.v.listaPostes[i].images.img7=dirArchivo;
                    break;
                  default:
                }
                console.log('listaPostes despues de subir archivo ', ctrl.v.listaPostes);
              } // fin if dirArchivo
            } // fin if
        }
    };

      var files = $("#"+elemid).get(0).files;
      var uploadDir = ctrl.v.folder;
      if (files.length > 0){
        // create a FormData object which will be sent as the data payload in the
        // AJAX request
        var formData = new FormData();

        // loop through all the selected files and add them to the formData object
        for (var i = 0; i < files.length; i++) {
          var file = files[i];

          // add the files to formData object for the data payload
          formData.append('uploads[]', file, file.name);
        }
        formData.append('chucha', uploadDir);
        console.log("formData::",formData);
        $("input:file").prop('disabled', true);
        $.ajax({
          url: '/img4kmz',
          type: 'POST',
          data: formData,
          processData: false,
          contentType: false,
          success: function(data){
            if (data.success) {
              console.log('upload successful!\n' , data);
              $("#"+elemid).hide();
              var fillename = data.filename;
              $('#progress-'+elemid).text(fillename);
              //$.post( "img4kmz/move/:filename/:directorio", function( data ) {
              console.log("fillename&folder :: ", fillename, ctrl.v.folder);
              $.ajax({
                url: "/img4kmz/move",
                type: "POST",
                data: { filename: fillename,
                        directorio: ctrl.v.folder },
                success: function( data ) {
                  console.log( "data que llego: ", data );
                  findIndex(data.ruta, ord);
                  $("input:file").prop('disabled', false);
                },
                error : function (err) {
                  console.log(err);
                  $("input:file").prop('disabled', false);
                }
              });

            } else {
              $('#progress-'+elemid).text('0%');
              $('#progress-'+elemid).width('0%');
              $("input:file").prop('disabled', false);
            }
          },
          xhr: function() {
            // create an XMLHttpRequest
            var xhr = new XMLHttpRequest();

            // listen to the 'progress' event
            xhr.upload.addEventListener('progress', function(evt) {

              if (evt.lengthComputable) {
                // calculate the percentage of upload completed
                var percentComplete = evt.loaded / evt.total;
                percentComplete = parseInt(percentComplete * 100);

                // update the Bootstrap progress bar with the new percentage
                $('#progress-'+elemid).text(percentComplete + '%');
                $('#progress-'+elemid).width(percentComplete + '%');

                // once the upload reaches 100%, set the progress bar text to done
                if (percentComplete === 100) {
                  $('#progress-'+elemid).html('Done');
                }

              }

            }, false);

            return xhr;
          }
          , error : function (error) {
            console.log(error);
            $("input:file").prop('disabled', false);
          }
        });

      }
    //});
  };

  /* borrar archivo
  * elimina el archivo, solicitando al servidor
  * envia por http/delete el nombre del archivo
  */
  ctrl.ba = function (imgIndex, obj, ind) {
    console.log("estoy en el indice ",imgIndex);
    console.log("el poste/mufa/cliente es: ", obj);
    var nombreArchivo = "";
    switch (imgIndex) {
      case 1:
        nombreArchivo = obj.images.img1;
        break;
      case 2:
        nombreArchivo = obj.images.img2;
        break;
      case 3:
        nombreArchivo = obj.images.img3;
        break;
      case 4:
        nombreArchivo = obj.images.img4;
        break;
      case 5:
        nombreArchivo = obj.images.img5;
        break;
      case 6:
        nombreArchivo = obj.images.img6;
        break;
      case 7:
        nombreArchivo = obj.images.img7;
        break;
      default:
    }
    console.log("el archivo es:",nombreArchivo);
    FileS.delete(nombreArchivo)
    .then(function (data) {
      //console.log("dentro de then, data es: ", data);
        switch (imgIndex) {
          case 1:
            obj.images.img1="";
            break;
          case 2:
            obj.images.img2="";
            break;
          case 3:
            obj.images.img3="";
            break;
          case 4:
            obj.images.img4="";
            break;
          case 5:
            obj.images.img5="";
            break;
          case 6:
            obj.images.img6="";
            break;
          case 7:
            obj.images.img7="";
            break;
          default:
        }
    })
    .catch(function (error) {
      console.log(error);
    });


  };
  // fin ctrl.ba = function

}) // --fin controlador paso4Ctrl

.controller("paso5Ctrl", function ($scope, $http, DataFc, $rootScope) {
  var ctrl = this;
  ctrl.v = DataFc.data;
  ctrl.idreporte = DataFc.idreporte;

  ctrl.la = [];
  ctrl.lai = {};

  if (ctrl.v && ctrl.v.listaActividades) {
    ctrl.la = ctrl.v.listaActividades;
  }

  ctrl.initLai = function () {
      ctrl.lai = {
        cantidad:"",
        item:"",
        descripcionItem:"",
        conIva:"",
        totalSinIva:"",
        totalConIva:""
      };
  };
  ctrl.addlai = function () {
    ctrl.la.push(ctrl.lai);
    ctrl.initLai();
  };

  ctrl.submit = function () {
    ctrl.v.listaActividades = ctrl.la;
    DataFc.data = ctrl.v;
    console.log(DataFc.data);
  };

  $scope.probarZip = function () {
    if (ctrl.idreporte && ctrl.v) {
      $http.get("/apikmz/download/"+ctrl.idreporte)
      .then( function (res) {
        if (res.data.success) {
          ctrl.kmz = res.data.success;
          ctrl.pathKmz = res.data.pathKmz;
          console.log(res.data);
        } else {
          ctrl.errorKmz=true;
        }
      });
    }
  };
})

.factory("DataFc", function () {
  var obj = {
    // paso1
    empresa : "",
    instalacion : "",
    ot : "",
    nombreCliente : "",
    direccion : "",
    codigoLargoMufaNodo : "",
    idmufa : "",
    codigoPuertoSplitter : "",
    fechaInstalacion : "",
    secuenciaInicial : "",
    secuenciaFinal : "",
    hiloProcedencia : "",
    hiloDestino : "",

    // paso2
    habilitacion : false,
    enrutado: false,
    tendido: false,
    conectorizacion: false,
    factibilidadDeServicio: false,
    postes: false,
    retiroFO: false,
    ducto: false,
    instalacionCT: false,
    traslado: false,

    // paso3
    fechainicio : null,
    fechafin : null,
    listaPostes : [],

    // paso4
    listaActividades : []
  };
  return {
    data : obj,
    idreporte : ""
  };
})

.factory("FileS", function ($http) {

  var deleteFilePromise = function (filename) {
      return $http.post("/apikmz/unlinkFile", {filename: filename});
  };

  return {
    delete : deleteFilePromise
  };

});
